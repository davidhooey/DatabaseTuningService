# Statistics

The query planner requires table level statistics in order to make good query
plans.
The auto vacuum process will automatically generate statistics.

## Updating Statistics

[PostgreSQL: Documentation: ANALYZE](https://www.postgresql.org/docs/13/sql-analyze.html)

Analyze all tables in current database

```sql
analyze verbose
```

Analyze a single table.

```sql
analyze verbose dauditnewcore
```

Analyze columns.

```sql
analyze verbose dauditnewcore (eventid)
analyze verbose dauditnewcore (eventid, dataid)
```

**VERBOSE enables display of progress messages.**

## Last Stats Update

Shows the last time each table has been analyzed, manually or automatically, the number of modifications (insert update, delete) since last analyze. Lastly, the number of live and dead rows is shown and the percetage of dead rows is calculated.

```sql
select
    relname,
    last_analyze,
    last_autoanalyze,
    analyze_count,
    autoanalyze_count,
    n_mod_since_analyze,
    n_live_tup,
    n_dead_tup,
    case
        when n_live_tup > 0 then
            round(n_dead_tup*1.0/n_live_tup*100, 2)
        else
            0
    end as pct_dead_tup
from
    pg_stat_user_tables
order by
    relname;
```

## default_statistics_target

Indicates how many values are stored in the list of most common values and
indicates the number of rows to be inspected by ANALYZE.

Raising the limit may allow more accurate query planner estimates particularly
for columns with irregular data distributions. More space is used to store the
statistic and slightly more time to compute.

The default is 100 with a maximum of 10,000.

The number of rows to be analyzed is determine by:

```
300 * default_statistics_target
```

[PostgreSQL: Documentation: Query Planning](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-DEFAULT-STATISTICS-TARGET)

Explicitly set statistics target value for a column.

```sql
alter table dtreecore alter column subtype set statistics 1000;
```

Viewing column settings.

```sql
select
    c.relname,
    a.attname,
    a.attstattarget,
    a.attoptions
from
    pg_class c
    inner join
    pg_attribute a
        on c.oid = a.attrelid
where
    c.relname = 'table_name'
order by
    c.relname,
    a.attname;
```

## Viewing Statistics

The view `pg_stats` provides access to the information stored in the  `pg_statistic` catalog.

[PostgreSQL: Documentation: pg_stats](https://www.postgresql.org/docs/current/view-pg-stats.html)

Show statistics for a single table.

```sql
select * from pg_stats where tablename = 'table_name'
```

Show statistics for all tables in a schema.

```sql
select * from pg_stats where schemaname = 'public' order by tablename, attname
```
