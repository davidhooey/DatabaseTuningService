select
    s.relname as table_name,
    s.indexrelname as index_name,
    i.indisunique as is_unique,
    s.idx_scan as index_scans
from
    pg_stat_user_indexes s
    inner join
    pg_index i
        on i.indexrelid = s.indexrelid
where
    s.idx_scan < 5
order by
    s.relname,
    s.indexrelname;
