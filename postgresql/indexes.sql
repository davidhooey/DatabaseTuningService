select
    t.relname as table_name,
    i.relname as index_name,
    ix.indisunique as is_unique,
    am.amname as index_type,
    array_to_string(array_agg(a.attname order by array_position(ix.indkey, a.attnum)), ', ') as column_names,
    ix.indnatts as number_of_columns,
    ix.indnkeyatts as number_of_key_columns,
    ix.indisvalid as is_valid
from
    pg_index ix
    inner join
    pg_class t
        on t.oid = ix.indrelid 
    inner join
    pg_class i
        on i.oid = ix.indexrelid 
    inner join
    pg_attribute a
        on a.attrelid = t.oid
        and a.attnum = ANY(ix.indkey)
    inner join
    pg_am am
        on am.oid = i.relam
where
    t.relkind = 'r'
group by
    t.relname,
    i.relname,
    ix.indisunique,
    am.amname,
    ix.indnatts,
    ix.indnkeyatts,
    ix.indisvalid
order by
    t.relname,
    i.relname;
