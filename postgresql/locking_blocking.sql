select
    blocking_locks.pid as blocking_pid,
    blocking_activity.usename as blocking_user,
    blocking_activity.state as blocking_state,
    blocking_activity.client_addr as blocking_ip,
    blocking_activity.client_hostname as blocking_hostname,
    blocking_activity.xact_start as blocking_xact_start,
    ((date_part('day', now()::timestamp - blocking_activity.xact_start) * 24 +
     date_part('hour', now()::timestamp - blocking_activity.xact_start)) * 60 +
     date_part('minute', now()::timestamp - blocking_activity.xact_start)) * 60 +
     date_part('second', now()::timestamp - blocking_activity.xact_start) as blocking_xact_secs,
    blocking_activity.query as blocking_sql,
    ' -> ' as is_blocking,
    blocked_locks.pid as blocked_pid,
    blocked_activity.usename as blocked_user,
    blocked_activity.state as blocked_state,
    blocked_activity.client_addr as blocked_ip,
    blocked_activity.client_hostname as blocked_hostname,
    blocked_activity.xact_start as blocked_xact_start,
    blocked_activity.query_start as blocked_query_start,
    ((date_part('day', now()::timestamp - blocked_activity.xact_start) * 24 +
     date_part('hour', now()::timestamp - blocked_activity.xact_start)) * 60 +
     date_part('minute', now()::timestamp - blocked_activity.xact_start)) * 60 +
     date_part('second', now()::timestamp - blocked_activity.xact_start) as blocked_xact_secs,
    ((date_part('day', now()::timestamp - blocked_activity.query_start) * 24 +
     date_part('hour', now()::timestamp - blocked_activity.query_start)) * 60 +
     date_part('minute', now()::timestamp - blocked_activity.query_start)) * 60 +
     date_part('second', now()::timestamp - blocked_activity.query_start) as blocked_query_secs,
    blocked_activity.query as blocked_sql
from
    pg_catalog.pg_locks blocked_locks
    join
    pg_catalog.pg_stat_activity blocked_activity
        on blocked_activity.pid = blocked_locks.pid
    join
    pg_catalog.pg_locks blocking_locks
        on blocking_locks.locktype = blocked_locks.locktype
        and blocking_locks.database is not distinct from blocked_locks.database
        and blocking_locks.relation is not distinct from blocked_locks.relation
        and blocking_locks.page is not distinct from blocked_locks.page
        and blocking_locks.tuple is not distinct from blocked_locks.tuple
        and blocking_locks.virtualxid is not distinct from blocked_locks.virtualxid
        and blocking_locks.transactionid is not distinct from blocked_locks.transactionid
        and blocking_locks.classid is not distinct from blocked_locks.classid
        and blocking_locks.objid is not distinct from blocked_locks.objid
        and blocking_locks.objsubid is not distinct from blocked_locks.objsubid
        and blocking_locks.pid != blocked_locks.pid
    join
    pg_catalog.pg_stat_activity blocking_activity
        on blocking_activity.pid = blocking_locks.pid
where
    not blocked_locks.granted
order by
    blocked_query_secs desc;
