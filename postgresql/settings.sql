-- Show all settings.
show all;

-- Show single setting.
show shared_buffers;

-- All setting information.
select
    name,
    setting,
    unit,
    category,
    short_desc,
    extra_desc,
    context,
    vartype,
    source,
    min_val,
    max_val,
    enumvals,
    boot_val,
    reset_val,
    sourcefile,
    sourceline,
    pending_restart
from
    pg_settings
order by
    name;
