# pg_stat_statements

The pg_stat_statements module enables tracking of SQL statements executed by the server.

[PostgreSQL: Documentation: pg_stat_statements](https://www.postgresql.org/docs/current/pgstatstatements.html)

## Enabling

1. Edit `postgresql.conf` and enable the `pg_stat_statements` extension, and
   set parameters if the defaults values are not sufficient.

    ```ini
    shared_preload_libraries = 'pg_stat_statements'

    # Default parameters values.
    pg_stat_statements.max = 5000
    pg_stat_statements.track = top
    pg_stat_statements.track_utility = on
    pg_stat_statements.track_planning = off
    pg_stat_statements.save = on
    ```

    * `pg_stat_statements.max` (integer)
      * The maximum number of statements tracked by the module.
      * Default: `5000`
    * `pg_stat_statements.track` (enum)
      * Controls which statements are counted by the module.
      * `top`
        * Track top-level statements (those issued directly by clients).
      * `all`
        * Track nested statements (such as statements invoked within functions).
      * `none`
        * Disable statement statistics collection.
      * Default: `top`
      * Only superusers can change this setting.
    * `pg_stat_statements.track_utility` (boolean)
      * Controls whether utility commands are tracked by the module. Utility
        commands are all those other than SELECT, INSERT, UPDATE and DELETE.
      * Default: `on`
      * Only superusers can change this setting.
    * `pg_stat_statements.track_planning` (boolean)
      * Controls whether planning operations and duration are tracked by the
        module. Enabling this parameter may incur a noticeable performance penalty,
        especially when a fewer kinds of queries are executed on many concurrent
        connections.
      * Default: `off`
      * Only superusers can change this setting.
    * `pg_stat_statements.save` (boolean)
      * Specifies whether to save statement statistics across server shutdowns. If
        it is `off` then statistics are not saved at shutdown nor reloaded at
        server start.
      * Default: `on`
      * This parameter can only be set in the `postgresql.conf` file or on the
        server command line.

2. Restart PostgreSQL.

    ```bash
    pg_ctl restart -D /path_to_pgdata/data
    ```

3. Create the extension.

    ```sql
    create extension pg_stat_statements;
    ```

## Querying

Statements are collected in `pg_stat_statements` with their performance data.

### Columns:

`userid` OID of user who executed the statement

`dbid` OID of database in which the statement was executed

`queryid` Internal hash code, computed from the statement’s parse tree

`query` Text of a representative statement

`calls` Number of times executed

`total_time` Total time spent in the statement, in milliseconds

`min_time` Minimum time spent in the statement, in milliseconds

`max_time` Maximum time spent in the statement, in milliseconds

`mean_time` Mean time spent in the statement, in milliseconds

`stddev_time` Population standard deviation of time spent in the statement, in milliseconds

`rows` Total number of rows retrieved or affected by the statement

`shared_blks_hit` Total number of shared block cache hits by the statement

`shared_blks_read` Total number of shared blocks read by the statement

`shared_blks_dirtied` Total number of shared blocks dirtied by the statement

`shared_blks_written` Total number of shared blocks written by the statement

`local_blks_hit` Total number of local block cache hits by the statement

`local_blks_read` Total number of local blocks read by the statement

`local_blks_dirtied` Total number of local blocks dirtied by the statement

`local_blks_written` Total number of local blocks written by the statement

`temp_blks_read` Total number of temp blocks read by the statement

`temp_blks_written` Total number of temp blocks written by the statement

`blk_read_time` Total time the statement spent reading blocks, in milliseconds (if  [track_io_timing](https://www.postgresql.org/docs/10/runtime-config-statistics.html#GUC-TRACK-IO-TIMING)  is enabled, otherwise zero)

`blk_write_time` Total time the statement spent writing blocks, in milliseconds (if  [track_io_timing](https://www.postgresql.org/docs/10/runtime-config-statistics.html#GUC-TRACK-IO-TIMING)  is enabled, otherwise zero)

Show queries from a particular database.

```sql
select
    s.*
from
    pg_stat_statements s
    inner join
    pg_database d
        on s.dbid = d.oid
where
    d.datname = 'cs1628'
order by
    total_time desc
```

```text
Name                | Value
--------------------|----------------------------------------------------
userid              | 16415
dbid                | 16396
queryid             | 3005214073
query               | UPDATE DTreeCore SET Deleted = $2 WHERE DataID = $1
calls               | 1844
total_time          | 1866.8169900000003
min_time            | 0.052079
max_time            | 224.07873
mean_time           | 1.0123736388286355
stddev_time         | 7.445661083133956
rows                | 1844
shared_blks_hit     | 92077
shared_blks_read    | 690
shared_blks_dirtied | 830
shared_blks_written | 0
local_blks_hit      | 0
local_blks_read     | 0
local_blks_dirtied  | 0
local_blks_written  | 0
temp_blks_read      | 0
temp_blks_written   | 0
blk_read_time       | 0.0
blk_write_time      | 0.0
```

## Functions

### `pg_stat_statements_reset()`

Discards all statistics gathered so far by `pg_stat_statements`. By default,
this function can only be executed by superusers.

```sql
select pg_stat_statements_reset()
```

### `pg_stat_statements(showtext boolean)`

The `pg_stat_statements` view is defined in terms of a function also named
`pg_stat_statements`. It is possible for clients to call the
pg_stat_statements function directly, and by specifying `showtext := false`
have query text be omitted.

```sql
select * from pg_stat_statements(showtext := false)
```
