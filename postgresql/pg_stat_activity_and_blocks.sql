select
    activity.pid,
    activity.datid,
    activity.datname,
    activity.state,
    activity.usesysid,
    activity.usename,
    activity.application_name,
    activity.client_addr,
    activity.client_hostname,
    activity.query_start,
    ((date_part('day', now()::timestamp - activity.query_start) * 24 +
      date_part('hour', now()::timestamp - activity.query_start)) * 60 +
      date_part('minute', now()::timestamp - activity.query_start)) * 60 +
      date_part('second', now()::timestamp - activity.query_start) as query_secs,
    activity.xact_start,
    ((date_part('day', now()::timestamp - activity.xact_start) * 24 +
      date_part('hour', now()::timestamp - activity.xact_start)) * 60 +
      date_part('minute', now()::timestamp - activity.xact_start)) * 60 +
      date_part('second', now()::timestamp - activity.xact_start) as xact_secs,
    activity.wait_event_type,
    activity.wait_event,
    activity.query,
    ' -> ' as is_blocked_by,
    blocked.datid as blocked_datid,
    blocked.datname as blocked_datname,
    blocked.blocking_pid as blocking_pid,
    blocked.state as blocked_state,
    blocked.usesysid as blocked_usesysid,
    blocked.usename as blocked_usename,
    blocked.application_name as blocked_application_name,
    blocked.client_addr as blocked_client_addr,
    blocked.client_hostname as blocked_client_hostname,
    blocked.query_start as blocked_query_start,
    ((date_part('day', now()::timestamp - blocked.query_start) * 24 +
      date_part('hour', now()::timestamp - blocked.query_start)) * 60 +
      date_part('minute', now()::timestamp - blocked.query_start)) * 60 +
      date_part('second', now()::timestamp - blocked.query_start) as blocked_query_secs,
    blocked.xact_start as blocked_xact_start,
    ((date_part('day', now()::timestamp - blocked.xact_start) * 24 +
      date_part('hour', now()::timestamp - blocked.xact_start)) * 60 +
      date_part('minute', now()::timestamp - blocked.xact_start)) * 60 +
      date_part('second', now()::timestamp - blocked.xact_start) as blocked_xact_secs,
    blocked.wait_event_type as blocked_wait_event_type,
    blocked.wait_event as blocked_wait_event,
    blocked.transactionid as blocked_transactionid,
    blocked.mode as blocked_mode,
    blocked.query as blocked_sql
from
    pg_stat_activity activity
    left join
    (
        select
            blocking_locks.pid as blocking_pid,
            blocked_locks.pid as blocked_pid,
            blocking_activity.datid as datid,
            blocking_activity.datname as datname,
            blocking_activity.state as state,
            blocking_activity.usesysid as usesysid,
            blocking_activity.usename as usename,
            blocking_activity.application_name as application_name,
            blocking_activity.client_addr as client_addr,
            blocking_activity.client_hostname as client_hostname,
            blocking_activity.query_start as query_start,
            blocking_activity.xact_start as xact_start,
            blocking_activity.wait_event_type as wait_event_type,
            blocking_activity.wait_event as wait_event,
            blocking_locks.transactionid as transactionid,
            blocking_locks.mode as mode,
            blocking_activity.query as query
        from
            pg_locks blocked_locks
            join
            pg_stat_activity blocked_activity
                on blocked_activity.pid = blocked_locks.pid
            join
            pg_locks blocking_locks
                on blocking_locks.locktype = blocked_locks.locktype
                and blocking_locks.database is not distinct from blocked_locks.database
                and blocking_locks.relation is not distinct from blocked_locks.relation
                and blocking_locks.page is not distinct from blocked_locks.page
                and blocking_locks.tuple is not distinct from blocked_locks.tuple
                and blocking_locks.virtualxid is not distinct from blocked_locks.virtualxid
                and blocking_locks.transactionid is not distinct from blocked_locks.transactionid
                and blocking_locks.classid is not distinct from blocked_locks.classid
                and blocking_locks.objid is not distinct from blocked_locks.objid
                and blocking_locks.objsubid is not distinct from blocked_locks.objsubid
                and blocking_locks.pid != blocked_locks.pid
            join
            pg_stat_activity blocking_activity
                on blocking_activity.pid = blocking_locks.pid
        where
            not blocked_locks.granted
    ) blocked
        on blocked.blocked_pid = activity.pid
where
    activity.pid != pg_backend_pid()
and
    activity.state in('active', 'idle in transaction')
and
    (
        activity.query_start <= now() - interval '10 seconds'
        or
        activity.xact_start <= now() - interval '10 seconds'
    )
order by
    query_secs desc;
