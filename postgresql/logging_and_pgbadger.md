# Logging and pgBadger

Log PostgreSQL activity for SQL performance, auto vacuum, checkpoints,
connections, disconnect, locking and temp files.

Generate a analysis report using pgBadger.

## Configure Logging Setting

By default the PostgreSQL logs are generated within the cluster's `data`
directory. Having the logs files within the `data` directory will increase the
size of the backups. Consider placing the logs in a dedicated directory
outside of the `data` directory. To do so, specify an absolute path for the
`log_directory` setting.

The `log_filename` format will create rolling logs based on the week day and
hour. With this rotation any hour within the past week can be analyzed.

```text
postgresql_3_Wed_23.log
postgresql_4_Thu_00.log
...
postgresql_4_Thu_08.log
postgresql_4_Thu_09.log
```

```conf
log_autovacuum_min_duration = 0
log_checkpoints = on
log_connections = on
log_destination = 'stderr'
log_directory = '/var/lib/pgsql/10/pg_logs'
log_disconnections = on
log_duration = off
log_filename = 'postgresql_%w_%a_%H.log'
log_line_prefix = '%t [%p]: [%l] %quser=%u,db=%d,app=%a,client=%h '
log_lock_waits = on
log_min_duration_statement = 0
log_rotation_age = 60
log_rotation_size = 0
log_statement = 'none'
log_temp_files = 0
log_truncate_on_rotation = 'on'
logging_collector = 'on'
```

## Configure auto_explain

In the `postgresql.conf` enable the `auto_explain` module by adding it to the
`shared_preload_libraries` setting, and configure the `auto_explain` settings.

```conf
shared_preload_libraries = 'auto_explain'

auto_explain.log_analyze = 1
auto_explain.log_buffers = 1
auto_explain.log_timing = 1
auto_explain.log_triggers = 1
auto_explain.log_verbose = 1
auto_explain.log_format = text
auto_explain.log_min_duration = 3000
auto_explain.log_nested_statements = 1
auto_explain.sample_rate = 1
```

## Restart/Reload PostgreSQL

The `logging_collector` setting is the only log setting requiring a restart.
All other log settings can be reloaded.

Reload:

```bash
pg_ctl -D /var/lib/pgsql/10/data reload
```

Restart:

```bash
pg_ctl -D /var/lib/pgsql/10/data stop
pg_ctl -D /var/lib/pgsql/10/data start
```

### Log Compression

The following cron job can be run nightly to compress log files older than today.

```bash
0 0 * * * for f in $(find /path_to_logs/ -mtime +0 -name "postgresql_*.log"); do gzip -9 -f $f; done
```

## Install pgBadger

1. sudo yum install perl-devel
2. Download the latest tar ball from [pgBadger Releases](https://github.com/darold/pgbadger/releases).
3. tar xzf v11.x.tar.gz
4. cd pgbadger-11.x/
5. perl Makefile.PL
6. make && sudo make install

## Generating pgBadger Report

```bash
pgbadger --dbname cs16211 --top 50 --title cs16211 --outdir /var/lib/pgsql/10/pg_reports --outfile pgBadger_cs16211_20210203.html /var/lib/pgsql/10/pg_logs/postgresql-2021-02-03_104631.log
```

## PostgreSQL Log Entries

### Query Times

```log
2021-02-12 16:24:14 EST [7926]: [539] user=cs16211,db=cs16211,app=[unknown],client=10.5.102.141 LOG:  duration: 0.104 ms  bind PGCursor_0x7f27049e8570: UPDATE WorkerStatus SET AgentType = $1, LastUpdate = $2, LastStatus = $3, StatusStr = $4, Counter = $5  WHERE WorkerID = $6
```

### Query Plan

```log
2021-02-12 16:24:14 EST [7926]: [541] user=cs16211,db=cs16211,app=[unknown],client=10.5.102.141 LOG:  duration: 0.052 ms  plan:
    Query Text: UPDATE WorkerStatus SET AgentType = $1, LastUpdate = $2, LastStatus = $3, StatusStr = $4, Counter = $5  WHERE WorkerID = $6
    Update on public.workerstatus  (cost=20.26..24.28 rows=1 width=571) (actual time=0.043..0.044 rows=0 loops=1)
      Buffers: shared hit=5
      ->  Bitmap Heap Scan on public.workerstatus  (cost=20.26..24.28 rows=1 width=571) (actual time=0.026..0.029 rows=1 loops=1)
            Output: workerid, '0'::smallint, '2021-02-12 16:24:14'::timestamp(3) without time zone, 0, 'Sleeping for 0.5 seconds...'::character varying(255), '-1'::integer, ctid
            Recheck Cond: ((workerstatus.workerid)::text = 'Worker_monitoringagent_3099_002'::text)
            Heap Blocks: exact=1
            Buffers: shared hit=4
            ->  Bitmap Index Scan on workerstatus_1  (cost=0.00..20.26 rows=1 width=0) (actual time=0.019..0.019 rows=1 loops=1)
                  Index Cond: ((workerstatus.workerid)::text = 'Worker_monitoringagent_3099_002'::text)
                  Buffers: shared hit=3
```

### Autovacuum

```log
2021-03-24 13:00:27 EDT [11144]: [2] LOG:  automatic vacuum of table "cs1628.public.agentschedule": index scans: 1
        pages: 0 removed, 2 remain, 0 skipped due to pins, 0 skipped frozen
        tuples: 60 removed, 20 remain, 0 are dead but not yet removable, oldest xmin: 1100844226
        buffer usage: 104 hits, 0 misses, 2 dirtied
        avg read rate: 0.000 MB/s, avg write rate: 22.877 MB/s
        system usage: CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.00 s
2021-03-24 13:00:27 EDT [11144]: [3] LOG:  automatic analyze of table "cs1628.public.agentschedule" system usage: CPU: user: 0.00 s, system: 0.00 s, elapsed: 0.02 s
```

### Lock Waits

```log
2021-02-12 16:36:26 EST [9501]: [26] user=cs16211,db=cs16211,app=DBeaver 7.3.4 - SQLEditor <Script-16.sql>,client=10.2.72.178 LOG:  process 9501 still waiting for ShareLock on transaction 1013253691 after 1000.116 ms
```

### Checkpoint

```log
2021-03-24 13:07:20 EDT [1822]: [7247] LOG:  checkpoint starting: time
2021-03-24 13:07:40 EDT [1822]: [7248] LOG:  checkpoint complete: wrote 200 buffers (0.1%); 0 WAL file(s) added, 0 removed, 0 recycled; write=20.050 s, sync=0.001 s, total=20.057 s; sync files=125, longest=0.000 s, average=0.000 s; distance=2513 kB, estimate=3055 kB
```

### Connect/Disconnect

```log
2021-03-24 13:10:06 EDT [12615]: [1] user=[unknown],db=[unknown],app=[unknown],client=10.5.102.72 LOG:  connection received: host=10.5.102.72 port=48880
2021-03-24 13:10:06 EDT [12615]: [2] user=pghero,db=postgres,app=[unknown],client=10.5.102.72 LOG:  connection authorized: user=pghero database=postgres
2021-03-24 13:10:06 EDT [12614]: [28] user=pghero,db=postgres,app=bin/rake,client=10.5.102.72 LOG:  disconnection: session time: 0:00:00.091 user=pghero database=postgres host=10.5.102.72 port=48878
```
