select
    relname,
    heap_blks_read,
    heap_blks_hit,
    round(100 - ((heap_blks_read * 1.0) / nullif(heap_blks_hit, 0)) * 100, 4) heap_hit_ratio,
    idx_blks_read,
    idx_blks_hit,
    round(100 - ((idx_blks_read * 1.0) / nullif(idx_blks_hit, 0)) * 100, 4) idx_hit_ratio
from
    pg_statio_user_tables
order by
    relname;
