# PostgreSQL Vacuuming

PostgreSQL uses multi-version concurrency control (MVCC) to ensure data remains
consistent in high-concurreny environments. Each transaction operates on its
own snapshot of the database at the point in time in began, which means
outdated data cannot be deleted right away. Deleted rows are marked as dead
rows which are cleaned up by the vacuuming process.

Vacuuming optimizes performance by:

* Marking dead rows as available to store data, which speeds up sequential
  scans which cannot skip over dead rows.
* Updating a visibility map which keeps track of pages that don't contain
  deleted data.
* Preventing transaction ID wraparound failure.

The auto vacuuming feature also preiodically runs an ANALYZE to gather the
latest statistics for frequently updated tables.

The auto vacuum daemon relies on the statistics collector to determine when to
vacuum.

[PostgreSQL: Documentation: Routine Vacuuming](https://www.postgresql.org/docs/13/routine-vacuuming.html)

## Enabling

Ensure the statistics collector setting is on. The statistics collector updates
the number of dead rows for each table.

```sql
select name, setting from pg_settings where name = 'track_counts';
```

Enable the auto vacuum setting.

```sql
select name, setting from pg_settings where name = 'autovacuum';
```

Auto vacuum may be disabled at the table level.

```sql
select reloptions from pg_class where relname = 'table_name';
```

Enable auto vacuum at the table level.

```sql
alter table table_name set (autovacuum_enabled = true);
```

### Configuring

These settings control auto vacuum.

```sql
select * from pg_settings where category = 'Autovacuum';
```

```text
name                                | setting   | short_desc
------------------------------------|-----------|------------------------------------------------------------------------------------------
autovacuum                          | on        | Starts the autovacuum subprocess.
autovacuum_analyze_scale_factor     | 0.1       | Number of tuple inserts, updates, or deletes prior to analyze as a fraction of reltuples.
autovacuum_analyze_threshold        | 50        | Minimum number of tuple inserts, updates, or deletes prior to analyze.
autovacuum_freeze_max_age           | 200000000 | Age at which to autovacuum a table to prevent transaction ID wraparound.
autovacuum_max_workers              | 3         | Sets the maximum number of simultaneously running autovacuum worker processes.
autovacuum_multixact_freeze_max_age | 400000000 | Multixact age at which to autovacuum a table to prevent multixact wraparound.
autovacuum_naptime                  | 60        | Time to sleep between autovacuum runs.
autovacuum_vacuum_cost_delay        | 2         | Vacuum cost delay in milliseconds, for autovacuum.
autovacuum_vacuum_cost_limit        | -1        | Vacuum cost amount available before napping, for autovacuum.
autovacuum_vacuum_scale_factor      | 0.2       | Number of tuple updates or deletes prior to vacuum as a fraction of reltuples.
autovacuum_vacuum_threshold         | 50        | Minimum number of tuple updates or deletes prior to vacuum.
```

Configuring table level auto vacuum settings.

```sql
alter table table_name set (autovacuum_vacuum_scale_factor = 0.1);
```

View table level auto vacuum settings.

```sql
select reloptions from pg_class where relname = 'table_name';
```

## Monitoring

[PostgreSQL: Documentation: The Statistics Collector](https://www.postgresql.org/docs/9.3/monitoring-stats.html#PG-STATIO-ALL-TABLES-VIEW)

### Dead Rows

Tracking the number dead rows in each table will help determine if the vacuum
is working well.

```sql
select
    relname,
    n_dead_tup,
    n_live_tup,
    case
    when n_live_tup > 0 then
        round(n_dead_tup*1.0/n_live_tup*100, 2)
    else
        0
    end as pct_dead_tup
from
    pg_stat_user_tables
order by
    relname;
```

### Table Disk Usage

Tracking the amount of disk space used by each table can help detect vacuuming
issues. If a table is unexpectedly increasing in size it may indicate there is
a problem vacuuming that table.

```sql
select
    c.relname as table_name,
    pg_size_pretty(pg_table_size(c.oid)) as table_size
from
    pg_class c
    left join
    pg_namespace n
        on n.oid = c.relnamespace
where
    n.nspname not in('pg_catalog', 'information_schema')
and
    n.nspname !~ '^pg_toast'
and
    c.relkind in ('r')
order by
    pg_table_size(c.oid) desc
```

### Last Vacuum Time

Track the last time of a successful manual vacuum or auto vacuum.

```sql
select
    relname,
    last_vacuum,
    last_autovacuum,
    vacuum_count,
    autovacuum_count
from
    pg_stat_user_tables
order by
    relname;
```

### Vaccum Progress

The `pg_stat_progress_vacuum` shows all the current vacuum processes.

[PostgreSQL: Documentation: Progress Reporting](https://www.postgresql.org/docs/current/progress-reporting.html#VACUUM-PROGRESS-REPORTING)

```sql
select * from  pg_stat_progress_vacuum
```

### Manual Vacuum

[PostgreSQL: Documentation: VACUUM](https://www.postgresql.org/docs/13/sql-vacuum.html)

Vacuum all tables.

```sql
vacuum
```

Vacuum all tables and update statistics.

```sql
vacuum analyze
```

Vacuum single table.

```sql
vacuum table_name
```

Vacuum and reclaim all empty space (locks table).

```sql
vacuum full
```
