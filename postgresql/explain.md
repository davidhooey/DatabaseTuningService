# Explain

Generating query plans.

## Options

```text
EXPLAIN [ ( option [, …] ) ] sql_st atement

option

ANALYZE [ boolean ]                  Default: FALSE
VERBOSE [ boolean ]                  Default: FALSE
COSTS   [ boolean ]                  Default: TRUE
BUFFERS [ boolean ]                  Default: FALSE
TIMING  [ boolean ]                  Default: TRUE
SUMMARY [ boolean ]                  Default: included with ANALYZE
FORMAT  { TEXT | XML | JSON | YAML } Default: TEXT
```

## EXPLAIN

Generates query plan based on statistics only.

```sql
explain select * from dauditnewcore order by eventid
```

```text
QUERY PLAN
--------------------------------------------------------------------------------------------------
Index Scan using dauditnewcore_primary on dauditnewcore  (cost=0.29..2838.20 rows=63158 width=721)
```

## EXPLAIN ANALYZE

Runs the query and displays the query plan along with estimate and actual statistics.

```sql
explain analyze select * from dauditnewcore order by eventid
```

```text
QUERY PLAN
-------------------------------------------------------------------------------------------------------------------------------------------------
Index Scan using dauditnewcore_primary on dauditnewcore  (cost=0.29..2838.20 rows=63158 width=721) (actual time=0.034..11.369 rows=63154 loops=1)
Planning time: 0.124 ms
Execution time: 13.877 ms
```

### DML

Since `explain analyze` runs the query, wrap DML statements in a transaction.

```sql
begin;
    explain analyze sql_statement;
rollback;
```

## EXPLAIN ANALYZE BUFFERS

Generates the query plan along with estimate, actual and buffer reads.

```sql
explain (analyze, buffers, verbose) select * from dauditnewcore order by eventid
```

Buffers: shared read indicates physical IO

```text
QUERY PLAN
----------------------------------------------------------------------------------------------------------------------
Seq Scan on dauditnewcore  (cost=0.00..1802.58 rows=63158 width=721) (actual time=11.897..3102.066 rows=63154 loops=1)
  Buffers: shared read=1171 dirtied=89
Planning time: 68.910 ms
Execution time: 3105.104 ms
```

Buffers: shared hit indicates cache hits.

```text
QUERY PLAN
------------------------------------------------------------------------------------------------------------------
Seq Scan on dauditnewcore  (cost=0.00..1802.58 rows=63158 width=721) (actual time=0.011..5.330 rows=63154 loops=1)
  Buffers: shared hit=1171
Planning time: 0.084 ms
Execution time: 8.135 ms
```

### Cost and Actual

* `cost=0.00..1802.58 rows=63158 width=721`
  * `0.00` estimated cost to return the first row.
  * `1802.58` estimated cost to return the last row.
  * `rows=63158` estimated number of rows.
  * `with=721` estimated average row width in KB.
* `actual time=11.897..3102.066 rows=63154 loops=1`
  * `11.897` time in ms to return the first row.
  * `3102.066` time in ms to return the last row.
  * `rows=63154` the true number of rows.
  * `loops=1` number of times the sub plan was executed. Multiple with the last time value for total time.
* Times of upper  level nodes include the times of the lower level nodes.
