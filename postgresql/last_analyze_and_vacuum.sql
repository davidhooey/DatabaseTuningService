select
    relname,
    last_vacuum,
    last_autovacuum,
    vacuum_count,
    autovacuum_count,
    last_analyze,
    last_autoanalyze,
    analyze_count,
    autoanalyze_count,
    n_mod_since_analyze,
    n_live_tup,
    n_dead_tup,
    case
    when n_live_tup > 0 then
        round(n_dead_tup*1.0/n_live_tup*100, 2)
    else
        0
    end as pct_dead_tup
from
    pg_stat_user_tables
order by
    relname;
