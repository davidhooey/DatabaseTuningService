# Shared Buffers

The shared buffers increases performance by caching pages and reducing physical
IO.

## Current Size

```sql
show shared_buffers;
```

```sql
select name, setting, (setting::bigint * 8192) cache_size
from pg_settings
where name = 'shared_buffers';
```

## `pg_statio_user_tables`

The `pg_statio_user_tables` table shows the IO reads and shared buffer reads
for each of the user’s tables and its indexes. This data is the accumulation of
reads since PostgreSQL startup.

```sql
select
    relname,
    heap_blks_read,
    heap_blks_hit,
    round(100 - ((heap_blks_read * 1.0) / nullif(heap_blks_hit, 0)) * 100, 4) heap_hit_ratio,
    idx_blks_read,
    idx_blks_hit,
    round(100 - ((idx_blks_read * 1.0) / nullif(idx_blks_hit, 0)) * 100, 4) idx_hit_ratio
from
    pg_statio_user_tables
order by
    relname
```

```text
Name          |Value
--------------|---------
relname       |dtreecore
heap_blks_read|63
heap_blks_hit |909332
heap_hit_ratio|99.9931
idx_blks_read |156
idx_blks_hit  |897593
idx_hit_ratio |99.9826
```

### Shared Buffers for Workload

1. Determine current shared buffers performance data by querying
   `pg_statio_user_tables`.

2. Perform workload.

3. Determine updated shared buffers performance data by querying
   `pg_statio_user_tables`.

4. Calculate the difference.
