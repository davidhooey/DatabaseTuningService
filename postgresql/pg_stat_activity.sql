select
    datid,
    datname,
    pid,
    leader_pid,
    state,
    usesysid,
    usename,
    application_name,
    client_addr,
    client_hostname,
    query_start,
    ((date_part('day', now()::timestamp - query_start) * 24 +
      date_part('hour', now()::timestamp - query_start)) * 60 +
      date_part('minute', now()::timestamp - query_start)) * 60 +
      date_part('second', now()::timestamp - query_start) as query_secs,
    xact_start,
    ((date_part('day', now()::timestamp - xact_start) * 24 +
      date_part('hour', now()::timestamp - xact_start)) * 60 +
      date_part('minute', now()::timestamp - xact_start)) * 60 +
      date_part('second', now()::timestamp - xact_start) as xact_secs,
    wait_event_type,
    wait_event,
    query_id,
    query
from
    pg_stat_activity
where
    pid != pg_backend_pid()
and
    state in('active', 'idle in transaction');
