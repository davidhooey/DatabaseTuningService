# Table and Index Data Density

Routine vacuuming can free space but sometimes it may still be not enough. If
table or index files have grown in size, vacuum can clean up some space within
pages, but it can rarely reduce the number of pages. The reclaimed space can
only be returned to the operating system if several empty pages appear at the
very end of the file, which does not happen too often.

An excessive size can lead to unpleasant consequences:

* Full table (or index) scan will take longer.
* A bigger buffer cache may be required (pages are cached as a whole, so data density decreases).
* B-trees can get an extra level, which slows down index access.
* Files take up extra space on disk and in backups.

A low data density of a table or index will benefit from a full rebuild using `vacuum full table_name`.

## Create the `pgstattuple` extension.

```sql
create extension pgstattuple;
```

## Table Data Density

```sql
select * from pgstattuple('table_name');
```

```sql
=> select * from pgstattuple('sample_table') \gx
-[ RECORD 1 ]------+----------
table_len          | 965050368
tuple_count        | 5703312
tuple_len          | 900283576
tuple_percent      | 93.29
dead_tuple_count   | 3
dead_tuple_len     | 201
dead_tuple_percent | 0
free_space         | 4592360
free_percent       | 0.48
```

## Index Data Density

```sql
select * from pgstatindex('sample_table_idx');
```

```sql
=> select * from pgstatindex('sample_table_idx') \gx
-[ RECORD 1 ]------+---------
version            | 4
tree_level         | 2
index_size         | 41648128
root_block_no      | 245
internal_pages     | 38
leaf_pages         | 5045
empty_pages        | 0
deleted_pages      | 0
avg_leaf_density   | 90.19
leaf_fragmentation | 0k
```
