lock_details.sql
================

Details on current locks within the application database.


### Column Details

Column                | Description
--------------------- | -------------------------------------------------------
request_session_id    | Session ID that currently owns this request.
database_name         | The database name where the lock is being held.
resource_type         | Request type.
requested_object_name | The object name of the resource being locked.
request_mode          | Request mode for the granted or requested lock.
request_status        | Status of the request. Granted, Convert, or Wait.
blocking_session_id   | ID of the session that is blocking this request. If NULL, not blocked.
login_name            | Login of the user who owns this request.
client_net_address    | IP address of the connection making the request.
resource_description  | Description of the resource being consumed.
statement             | Query making the request.


### Required Permission

VIEW SERVER STATE
