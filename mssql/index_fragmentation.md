index_fragmentation.sql
=======================

List the indexes in descending order by fragmentation. If average fragmentation
is greater than or equal to 40, then rebuild the index, otherwise reorganize
the index. Statistics will need to be updated for reorganize operations as they
are not automatically updated.

Updates to existing rows can cause page splitting for indexes. When an update
causes the row to no longer fit within the given page, SQL Server will split
the page into two new pages and place half the rows in each. For heaps, SQL
Server will move the row to a new page and place a pointer in the original
page. For indexes receiving a high number of updates, consider a lower fill
factor to allow for row growth and reduce page splits.

When pages are out of order on disk due to page splits, this is known as
external fragmentation. Order by `avg_fragmentation_in_percent` descending to
show external fragmentation.

Empty space within a page is an indication of internal fragmentation. Order by
`avg_page_space_used_in_percent` ascending to show internal fragmentation. To
reduce internal fragmentation, the index needs to be rebuilt as opposed to
reorganized.


### Column Details

Column                         | Description
------------------------------ | ------------------------------------------------
database_name                  | Database name.
object_name                    | Object name.
index_name                     | Index name.
index_type_desc                | Index type, CLUSTERED or NONCLUSTERED.
partition_number               | Partition number.
alloc_unit_type_desc           | Allocation unit type, IN_ROW_DATA, LOB_DATA, ROW_OVERFLOW_DATA.
index_depth                    | Number of index levels.
index_level                    | Current level of the index.
avg_fragmentation_in_percent   | Percentage of logical fragmentation for indexes (external fragmentation).
avg_page_space_used_in_percent | Percentage of space used in all pages (internal fragmentation).
avg_fragment_size_in_pages     | Average number of pages in one fragment in the leaf level.
fragment_count                 | Nummber of fragments in the leaf level.
page_count                     | Total number of index or data pages.
record_count                   | Total number of records.
min_record_size_in_bytes       | Minimum record size in bytes.
max_record_size_in_bytes       | Maximum record size in bytes.
avg_record_size_in_bytes       | Average record size in bytes.
recommendation                 | Percentation of fragmention > 40% rebuild else reorganize.


### Required Permissions

CONTROL permission on the specified object within the database.
VIEW SERVER STATE
