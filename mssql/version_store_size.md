version_store_size.sql
======================

When using Snapshot isolation mode a row version store is used to store
previous version of rows being updated. This allows read operations to use
the version store to provide a consistent view of the data. Having the benefit
of readers not blocking writers, and writers not blocking readers.

The tempdb is used for the version store. It may be useful to determine the
amount of version store being used by each database.


### Column Details

Column        | Description
------------- | ---------------------------------------------------------------
database_name | Database name.
total_bytes   | Total bytes currently being used by this database in the version store.


### Required Permission

VIEW SERVER STATE
