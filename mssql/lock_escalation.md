lock_esclations.sql
===================

Returns a list of objects where lock esclation has occurred. Lock escalation
occurs from a row or page to a object level lock.


### Column Details

Column                             | Description
---------------------------------- | ------------------------------------------
database_name                      | Database name.
object_name                        | Object name.
index_name                         | Index name.
partition_number                   | Partition number within the index or heap.
index_lock_promotion_attempt_count | Cumulative number of times the database engine tried to escalate locks.
index_lock_promotion_count         | Cumulative number of times the database engine escalated locks.
percent_success                    | Percentage of successful lock escalations.


### Required Permissions

CONTROL permission on the specified object within the database.
VIEW SERVER STATE
