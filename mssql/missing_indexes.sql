use ApplicationDatabase;

select
    (user_seeks + user_scans) * avg_total_user_cost * (avg_user_impact * 0.01) as index_advantage,
    dbmigs.last_user_seek,
    dbmid.statement as "table",
    dbmid.equality_columns,
    dbmid.inequality_columns,
    dbmid.included_columns,
    dbmigs.unique_compiles,
    dbmigs.user_seeks,
    dbmigs.user_scans,
    dbmigs.avg_total_user_cost,
    dbmigs.avg_user_impact
from
    sys.dm_db_missing_index_group_stats as dbmigs with (nolock)
    inner join
    sys.dm_db_missing_index_groups as dbmig with (nolock)
        on dbmigs.group_handle = dbmig.index_group_handle
    inner join
    sys.dm_db_missing_index_details as dbmid with (nolock)
        on dbmig.index_handle = dbmid.index_handle
where
    dbmid.database_id = db_id()
order by
    index_advantage desc;
