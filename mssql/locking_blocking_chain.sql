if object_id('tempdb..#blocks') is not null
    drop table #blocks
select
    spid,
    blocked,
    replace (replace (st.text, char(10), ' '), char (13), ' ' ) as batch
into
    #blocks
from
    sys.sysprocesses spr
    cross apply
    sys.dm_exec_sql_text(spr.sql_handle) st
go

with blocking_tree (spid, blocking_spid, [level], batch)
as
(
    select
        blc.spid,
        blc.blocked,
        cast (replicate ('0', 4-len (cast (blc.spid as varchar))) + cast (blc.spid as varchar) as varchar (1000)) as [level],
        blc.batch
    from
        #blocks blc
    where
        (blc.blocked = 0 or blc.blocked = spid)
    and
        exists (select * from #blocks blc2 where blc2.blocked = blc.spid and blc2.blocked <> blc2.spid)
    union all
    select
        blc.spid,
        blc.blocked,
        cast(bt.[level] + right (cast ((1000 + blc.spid) as varchar (100)), 4) as varchar (1000)) as [level],
        blc.batch
    from
        #blocks as blc
        inner join
        blocking_tree bt
            on blc.blocked = bt.spid
    where
        blc.blocked > 0
    and
        blc.blocked <> blc.spid
)
select
    N'' + isnull(replicate (N'|         ', len (level)/4 - 2),'') + case when (len(level)/4 - 1) = 0 then '' else '|------  ' end + cast (bt.spid as nvarchar (10)) as blocking_tree,
    spr.lastwaittype   AS [type],
    spr.loginame       AS [login_name],
    db_name(spr.dbid)  as [source_database],
    st.text            AS [sql_text],
    case when cur.sql_handle is null then '' else (select [text] from sys.dm_exec_sql_text (cur.sql_handle)) end as [cursor_sql_text],
    db_name(sli.rsc_dbid)  as [database],
    object_schema_name(sli.rsc_objid,sli.rsc_dbid) as [schema],
    object_name(sli.rsc_objid, sli.rsc_dbid) as [table],
    spr.waitresource   as [wait_resource],
    spr.cmd            as [command],
    spr.program_name   as [application],
    spr.hostname       as [hostname],
    spr.last_batch     as [last_batch_time]
from
    blocking_tree bt
    left outer join
    sys.sysprocesses spr
        on spr.spid = bt.spid
    cross apply
    sys.dm_exec_sql_text(spr.sql_handle) st
    left join
    sys.dm_exec_cursors(0) cur
        on cur.session_id = spr.spid
        and cur.fetch_status != 0
    join
    sys.syslockinfo sli
        on sli.req_spid = spr.spid
        and sli.rsc_type = 5
        and object_name(sli.rsc_objid, sli.rsc_dbid) is not null
order by
    level asc
