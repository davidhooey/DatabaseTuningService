database_details.sql
====================

Application database settings.


### Column Details

Column                          | Description
------------------------------- | -------------------------------------------------------
database_id                     | ID of the database.
name                            | Name of the database.
create_date                     | Date the database was created.
compatibility_level             | SQL Server compatible level.
collation_name                  | Default collation in the database.
snapshot_isolation_state_desc   | Description of snapshot isolation transcations being allowed (OFF, ON, IN_TRANSISTION_TO_ON, IN_TRANSISTION_TO_OFF)
is_read_committed_snapshot_on   | Is READ_COMMITTED_SNAPSHOT on (1) or off (0).
recovery_model_desc             | Recovery model FULL, BULK_LOGGED, or SIMPLE.
target_recovery_time_in_seconds | The estimated time to recover the database in seconds.
is_auto_close_on                | AUTO_CLOSE on (1) or off (0).
is_auto_shrink_on               | AUTO_SHRINK on (1) or off (0).
is_auto_create_stats_on         | AUTO_CREATE_STATISTICS on (1) or off (0).
is_auto_update_stats_on         | AUTO_UPDATE_STATISTICS on (1) or off (0).
is_auto_update_stats_async_on   | AUTO_UPDATE_STATISTICS_ASYNC on (1) or off (0).
is_ansi_null_default_on         | ANSI_NULL_DEFAULT on (1) or off (0).
is_ansi_nulls_on                | ANSI_NULLS on (1) or off (0).
is_parameterization_forced      | Parameterization FORCED (1) or SIMPLE (0).


### Required Permission

VIEW ANY DATABASE
