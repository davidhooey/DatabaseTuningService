select
    d.name,
    f.*
from
    sys.master_files f
    inner join
    sys.databases d
        on f.database_id = d.database_id
where
    d.name = 'tempdb';
