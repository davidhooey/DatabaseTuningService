use ApplicationDatabase;

select
    db_name(dios.database_id) as database_name,
    object_name(dios.object_id, dios.database_id) as object_name,
    i.name as index_name,
    dios.partition_number,
    dios.index_lock_promotion_attempt_count,
    dios.index_lock_promotion_count,
    (cast(dios.index_lock_promotion_count as real) / dios.index_lock_promotion_attempt_count) as percent_success
from
    sys.dm_db_index_operational_stats(db_id(), null, null, null) dios
    inner join
    sys.indexes i
        on dios.object_id = i.object_id
        and dios.index_id = i.index_id
where
    dios.index_lock_promotion_count > 0
order by
    index_lock_promotion_count desc;
