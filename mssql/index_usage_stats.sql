use ApplicationDatabase;

select
    db_name(dius.database_id) as database_name,
    dius.database_id,
    object_name(dius.object_id, dius.database_id) as table_name,
    dius.object_id as table_id,
    i.name as index_name,
    dius.index_id,
    i.type_desc as index_type,
    i.is_unique,
    dius.user_seeks,
    dius.user_scans,
    dius.user_lookups,
    dius.user_seeks + dius.user_scans + dius.user_lookups as user_reads,
    dius.user_updates as user_writes,
    dius.last_user_seek,
    dius.last_user_scan,
    dius.last_user_lookup,
    dius.last_user_update
from
    sys.dm_db_index_usage_stats dius
    inner join
    sys.indexes i
        on dius.object_id = i.object_id and dius.index_id = i.index_id
where
    dius.database_id = db_id()
and
    i.index_id <> 0
order by
    table_name,
    index_name;
