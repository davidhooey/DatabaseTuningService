connection_details.sql
======================

Provides details for each session created by the application. Performance
statistics cumulated for all requests since the session was opened.


### Column Details

Column                           | Description
-------------------------------- | -------------------------------------------------
program_name                     | Client program that initiated the request.
login_name                       | SQL Server login used for this execution.
host_name                        | Name of the client workstation.
client_net_address               | IP address of the client workstation.
session_id                       | Unique session identifier.
database_id                      | ID of the current database for this session (SQL Server 2012).
status                           | Session status, Running, Sleeping, Dormant, Preconnect.
cpu_time                         | CPU time in ms used by this session.
memory_usage                     | Number of 8KB pages of memory used by this session.
reads                            | Number of disk reads performed by this session.
writes                           | Number of disk writes performed by this session.
logical_reads                    | Number of logical reads performed by this session.
row_count                        | Number of rows returned by this session.
total_scheduled_time             | Total time in ms this session's requests were scheduled for execution.
total_elapsed_time               | Time in ms since this session was established.
last_request_start_time          | Time at which the last request on this session began.
last_request_end_time            | Time of the last completion of a request on this session.
transaction_isolation_level      | Transaction level of the session.
transaction_isolation_level_text | Transaction level of the session in text form.
open_transaction_count           | Number of open transactions (SQL Server 2012).


### Required Permission

VIEW SERVER STATE
