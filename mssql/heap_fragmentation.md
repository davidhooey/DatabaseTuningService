heap_fragmentation.sql
======================

List the tables in descending order by fragmentation. Row growth can cause
SQL Server to move the row to another page and use a pointer in the original
page. This increases the number I/O operations that are required to fetch the
row.

SQL Server 2008 introduced the `alter table tableName rebuild;` statement to
reduce heap fragmentation.

If Enterprise Edition is used, online rebuilds are supported with
`alter table tableName rebuild with (online = on);`.


### Column Details

Column                         | Description
------------------------------ | ------------------------------------------------
database_name                  | Database name.
object_name                    | Object name.
index_type_desc                | Index type of HEAP.
partition_number               | Partition number.
alloc_unit_type_desc           | Allocation unit type, IN_ROW_DATA, LOB_DATA, ROW_OVERFLOW_DATA.
avg_fragmentation_in_percent   | Percent of extent fragmentation for heaps.
avg_page_space_used_in_percent | Percentage of space used in all pages.
avg_fragment_size_in_pages     | Average number of pages in one fragment in the leaf level.
fragment_count                 | Nummber of fragments in the leaf level.
page_count                     | Total number of index or data pages.
forwarded_record_count         | Number of records in a heap that have forward pointers to another data location.
record_count                   | Total number of records.
min_record_size_in_bytes       | Minimum record size in bytes.
max_record_size_in_bytes       | Maximum record size in bytes.
avg_record_size_in_bytes       | Average record size in bytes.
recommendation                 | Recommended alter table statement.


### Required Permission

CONTROL permission on the specified object within the database.
VIEW SERVER STATE
