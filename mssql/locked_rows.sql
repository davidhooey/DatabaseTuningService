select
    t.*
from
    table_name (nolock) as t
    join
    sys.dm_tran_locks as dtl
        on t.%%lockres%% = dtl.resource_description;
