cached_query_stats.sql
======================

Individual query plan statistics from the query cache against a given
database. Most times are in microseconds but are only accurate to
milliseconds.

### Columns

Column                        | Description
------------------------------| ------------------------------------------------------
dbid                          | Application database ID.
name                          | Application database name.
sql_handle                    | SQL handle for this statement.
text                          | SQL text.
execution_count               | Number of executions since the plan was last compiled.
creation_time                 | Time at which the plan was compiled.
last_execution_time           | Last time at which the plan started executing.
total_elapsed_time            | Total elapsed time for completed executions in microseconds.
average_elapsed_time          | Average elapsed time for completed executions in microseconds.
last_elapsed_time             | Elapsed time for the most recently completed execution in microseconds.
min_elapsed_time              | Minimum elapsed time for completed executions in microseconds.
max_elapsed_time              | Maximum elapsed time for completed executions in microseconds.
total_rows                    | Total number of rows returned by the query.
average_rows                  | Average number of rows returned by the query.
last_rows                     | Number of rows returned by the last execution of the query.
min_rows                      | Minimum number of rows returned for completed executions.
max_rows                      | Maximum number of rows returned for completed executions.
total_worker_time             | Total CPU time consumed by executions of this plan in microseconds.
average_worker_time           | Average CPU time consumed by executions of this plan in microseconds.
last_worker_time              | CPU time consumed by the most recent execution of this plan in microseconds.
min_worker_time               | Minimum CPU time consumed by executions of this plan in microseconds.
max_worker_time               | Maximum CPU time consumed by executions of this plan in microseconds.
total_physical_reads          | Total physical disk reads by executions of this plan.
average_physical_reads        | Average physical disk reads by executions of this plan.
last_physical_reads           | Physical disk reads for the most recent execution of this plan.
min_physical_reads            | Minimum physical disk reads for all executions of this plan.
max_physical_reads            | Maximum physical disk reads for all executions of this plan.
total_logical_reads           | Total logical reads (memory) for all executions of this plan.
average_logical_reads         | Average logical reads for all executions of this plan.
last_logical_reads            | Logical reads for the most recent execution of this plan.
min_logical_reads             | Minimum logical reads for all executions of this plan.
max_logical_reads             | Maximum logical reads for all executions of this plan.
total_logical_writes          | Total logical writes (memory) for all executions of this plan.
average_logical_writes        | Average logical writes for all executions of this plan.
last_logical_writes           | Logical writes for the most recent execution of this plan.
min_logical_writes            | Minimum logical writes for all executions of this plan.
max_logical_writes            | Maximum logical writes for all executions of this plan.
plan_handle                   | Plan handle for this requests's SQL execution plan.
query_plan                    | Query plan in XML format.
remove_sql_handle_from_cache  | Statement to remove the SQL statement from the cache.
remove_plan_handle_from_cache | Statement to remove the plan from the cache.

### Required Permission

VIEW ANY DATABASE
VIEW SERVER STATE
