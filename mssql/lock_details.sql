select
    dtl.request_session_id,
    db_name(dtl.resource_database_id) as database_name,
    dtl.resource_type,
    case
    when dtl.resource_type in ('DATABASE', 'FILE', 'METADATA') then
        dtl.resource_type
    when dtl.resource_type = 'OBJECT' then
        object_name(dtl.resource_associated_entity_id, dtl.resource_database_id)
    when dtl.resource_type in ('KEY', 'PAGE', 'RID') then
        (select object_name(object_id) from sys.partitions where sys.partitions.hobt_id = dtl.resource_associated_entity_id)
    else
        'Unidentified'
    end as requested_object_name,
    dtl.request_mode,
    dtl.request_status,
    der.blocking_session_id,
    des.login_name,
    dec.client_net_address,
    dtl.resource_description,
    case dtl.request_lifetime
    when 0 then
        dest_r.text
    else
        dest_c.text
    end as statement
from
    sys.dm_tran_locks dtl
    left outer join
    sys.dm_exec_requests der
        on dtl.request_session_id = der.session_id
    inner join
    sys.dm_exec_sessions des
        on dtl.request_session_id = des.session_id
    inner join
    sys.dm_exec_connections dec
        on dtl.request_session_id = dec.most_recent_session_id
    outer apply
    sys.dm_exec_sql_text(dec.most_recent_sql_handle) as dest_c
    outer apply
    sys.dm_exec_sql_text(der.sql_handle) as dest_r
where
    dtl.resource_type <> 'DATABASE'
and
    db_name(dtl.resource_database_id) = 'ApplicationDatabase'
order by
    dtl.request_session_id