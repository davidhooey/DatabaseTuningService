tempdb_data_files.sql
=====================

Lists the data and log files associated to the TEMPDB database.


### Required Permission

The minimum permissions are CREATE DATABASE, ALTER ANY DATABASE, or
VIEW ANY DEFINITION.
