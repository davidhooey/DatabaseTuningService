use master;

-- Disable Query Store
alter database DatabaseName set query_store = off;
