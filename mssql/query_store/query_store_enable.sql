use master;

-- Enable Query Store
alter database DatabaseName set query_store = on;

-- Set Query Store Options
alter database DatabaseName set query_store
(
    operation_mode = read_write,
    data_flush_interval_seconds = 900,  -- Default 900 (15 min)
    interval_length_minutes = 30,       -- Default 60 min
    max_storage_size_mb = 500,          -- Default 100 mb
    query_capture_mode = all,           -- Default all
    size_based_cleanup_mode = off,      -- Default on
    cleanup_policy =
    (
        stale_query_threshold_days = 60 -- Default 30
    )
);
