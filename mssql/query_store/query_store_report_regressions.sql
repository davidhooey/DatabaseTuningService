-- Checks for regressions within the last 10 minutes.
select
    qsq.query_id,
    qsp1.plan_id as "plan_id_1",
    qsp2.plan_id as "plan_id_2",
    qsq.object_id,
    rs1.avg_duration as "avg_duration_1",
    rs2.avg_duration as "avg_duration_2",
    rs1.avg_logical_io_reads as "avg_logical_io_reads_1",
    rs2.avg_logical_io_reads as "avg_logical_io_reads_2",
    rsi1.start_time as "start_time_1",
    rsi2.start_time as "start_time_2",
    qst.query_sql_text,
    qsp1.query_plan as "query_plan_1",
    qsp2.query_plan as "query_plan_2"
from
    sys.query_store_query qsq
    inner join
    sys.query_store_query_text qst
        on qsq.query_text_id = qst.query_text_id
    inner join
    sys.query_store_plan qsp1
        on qsq.query_id = qsp1.query_id
    inner join
    sys.query_store_runtime_stats rs1
        on qsp1.plan_id = rs1.plan_id
    inner join
    sys.query_store_runtime_stats_interval rsi1
        on rs1.runtime_stats_interval_id = rsi1.runtime_stats_interval_id
    inner join
    sys.query_store_plan qsp2
        on qsq.query_id = qsp2.query_id
    inner join
    sys.query_store_runtime_stats rs2
        on qsp2.plan_id = rs2.plan_id
    inner join
    sys.query_store_runtime_stats_interval rsi2
        on rs2.runtime_stats_interval_id = rsi2.runtime_stats_interval_id
where
    rsi1.start_time > dateadd(minute, -10, getdate())
and
    rsi2.start_time > rsi1.start_time
and
    qsp1.plan_id <> qsp2.plan_id
and
    rs2.avg_duration > 2* rs1.avg_duration
order by
    qsq.query_id,
    rsi1.start_time,
    rsi2.start_time;
