use DatabaseName;

select
    q.query_id,
    qp.plan_id,
    count(*) plan_count,
    qt.query_sql_text
from
    sys.query_store_plan qp
    inner join
    sys.query_store_query q
        on qp.query_id = q.query_id
    inner join
    sys.query_store_query_text qt
        on q.query_text_id = qt.query_text_id
group by
    q.query_id,
    qp.plan_id,
    qt.query_sql_text
having
    count(*) > 1
order by
    q.query_id,
    qp.plan_id;
