use DatabaseName;

select
    q.query_id,
    qt.query_sql_text,
    qt.statement_sql_handle
from
    sys.query_store_query q
    inner join
    sys.query_store_query_text qt
        on q.query_text_id = qt.query_text_id
where
	qt.query_sql_text like '%query_text_search_string%';
