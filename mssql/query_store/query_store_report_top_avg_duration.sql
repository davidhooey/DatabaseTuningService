use DatabaseName;

select top 50
    q.query_id,
    qp.plan_id,
    qt.query_sql_text,
    cast(query_plan as xml) as 'query_plan',
    rsi.start_time,
    rsi.end_time,
    rs.first_execution_time,
    rs.last_execution_time,
    rs.count_executions,
    rs.avg_duration as 'avg_duration_us',
    rs.min_duration as 'min_duration_us',
    rs.max_duration as 'max_duration_us',
    rs.avg_cpu_time as 'avg_cpu_us',
    rs.min_cpu_time as 'min_cpu_time_us',
    rs.max_cpu_time as 'max_cpu_time_us',
    rs.avg_logical_io_reads,
    rs.min_logical_io_reads,
    rs.max_logical_io_reads,
    rs.avg_physical_io_reads,
    rs.min_physical_io_reads,
    rs.max_physical_io_reads,
    rs.avg_rowcount,
    rs.min_rowcount,
    rs.max_rowcount
from
    sys.query_store_plan qp
    inner join
    sys.query_store_query q
        on qp.query_id = q.query_id
    inner join
    sys.query_store_query_text qt
        on q.query_text_id = qt.query_text_id
    inner join
    sys.query_store_runtime_stats rs
        on qp.plan_id = rs.plan_id
    inner join
    sys.query_store_runtime_stats_interval rsi
        on rs.runtime_stats_interval_id = rsi.runtime_stats_interval_id
where
    rsi.start_time >= '2022-11-29 00:00:00'
and
    rsi.end_time <= '2022-11-30 00:00:00'
order by
    rs.avg_duration desc;
