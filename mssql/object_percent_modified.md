object_percent_modified.sql
===========================

Shows the percentage of row modifications since last statistics update.

### Column Details

Column            | Description
----------------- | ------------------------------------------------------------------
object_schema     | Object name.
table_name        | Table name.
index_name        | Index name.
indid             | Index ID.
rows_modified     | Number of rows modified.
row_count         | Total number of rows.
modified_percent  | Percentage of rows modified.
last_stats_update | Date of last statistics update.

### Required Permission

Query as database owner.
