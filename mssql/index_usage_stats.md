index_usage_stats.sql
=====================

Provides statistis on the usage of indexes. A server restart will flush this
view. The longer the server has been up, the more valuable this information is.


### Column Details

Column           | Description
---------------- | ------------------------------------------------------------
database_name    | Database name.
database_id      | ID of the database.
table_name       | Table name.
table_id         | ID of the table.
index_name       | Index name.
index_id         | ID of the index.
index_type       | Type of index, CLUSTERED or NONCLUSTERED.
is_unique        | Is the index unique (1) or not (0).
user_seeks       | Number of seeks by user queries.
user_scans       | Number of leaf node scans by user queries.
user_lookups     | Number of clustered index bookmark lookups by user queries.
user_reads       | Sum of user_seeks, user_scans, and user_lookups for a total number of reads.
user_writes      | Number of updates by user queries.
last_user_seek   | Time of last user seek.
last_user_scan   | Time of last user scan.
last_user_lookup | Time of last user lookup.
last_user_update | Time of last user update.


### Required Permission

VIEW SERVER STATE
