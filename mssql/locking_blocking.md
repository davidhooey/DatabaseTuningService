locking_blocking.sql
====================

Returns the locking and blocking sessions for a given database. Will list the
query doing the locking and the query being blocked.


### Column Details

Column                | Description
--------------------- | -------------------------------------------------------
resource_type         | The resource type being locked, File, Object, Page, Key, RID.
database_name         | The database name where the lock is being held.
requested_object_name | The object name of the resource being locked.
request_mode          | Request mode for the granted or requested lock.
request_status        | Status of the request. Granted, Convert, or Wait.
wait_duration_ms      | Total wait time for thsi wait type.
wait_type             | Name of the wait type.
blocked_session_id    | ID of the session that is being blocked.
blocked_user          | Login of user being blocked.
blocked_command       | Query of the user being blocked.
blocking_session_id   | ID of the session that is blocking the request. If NULL, not blocked.
blocking_user         | Login of the user blocking the request.
blocking_command      | Query of the user blocking the request.
resource_description  | Description of the resource being consumed.


### Required Permission

VIEW SERVER STATE
