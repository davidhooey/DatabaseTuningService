select
    getdate() time_now,
    des.session_id,
    des.status,
    case
    when der.blocking_session_id <> 0 and blocked.session_id is null then
        der.blocking_session_id
    when der.blocking_session_id <> 0 and des.session_id <> blocked.blocking_session_id then
        blocked.blocking_session_id
    when der.blocking_session_id = 0 and des.session_id = blocked.session_id then
        blocked.blocking_session_id
    when der.blocking_session_id <> 0 and des.session_id = blocked.blocking_session_id then
        der.blocking_session_id
    else
        null
    end as blocking_session_id,
    case
    when deqs.statement_start_offset is not null and deqs.statement_end_offset is not null then
        substring(
            dest.text,
            (deqs.statement_start_offset/2)+1,
            ((
                case deqs.statement_end_offset
                when -1 then
                    datalength(dest.text)
                else
                    deqs.statement_end_offset
                end - deqs.statement_start_offset
            )/2) + 1
        )
    else
        dest.text
    end as query_text,
    deqp.query_plan,
    der.total_elapsed_time total_elapsed_time_ms,
    der.total_elapsed_time/1000 total_elapsed_time_sec,
    der.total_elapsed_time/1000/60 total_elapsed_time_min,
    der.cpu_time,
    der.reads,
    der.writes,
    der.logical_reads,
    des.host_name,
    des.login_name,
    des.program_name,
    db_name(der.database_id) db_name,
    der.request_id,
    der.transaction_id,
    der.wait_type,
    der.wait_time,
    der.last_wait_type,
    der.wait_resource,
    der.open_transaction_count,
    der.open_resultset_count,
    der.transaction_isolation_level,
    case des.transaction_isolation_level
        when 0 then 'Unspecified'
        when 1 then 'ReadUncommitted'
        when 2 then 'ReadCommitted'
        when 3 then 'Repeatable'
        when 4 then 'Serializable'
        when 5 then 'Snapshot'
    end as transaction_isolation_level_text,
    der.row_count,
    dest.objectid,
    der.sql_handle,
    der.plan_handle
from
    sys.dm_exec_sessions as des
    left join
    sys.dm_exec_requests as der
        on der.session_id = des.session_id
    outer apply
    (
        select top 1
            dbid,
            last_batch,
            open_tran,
            sql_handle,
            session_id,
            blocking_session_id,
            lastwaittype,
            waittime
        from
        (
            select
                sys1.dbid,
                sys1.last_batch,
                sys1.open_tran,
                sys1.sql_handle,
                sys2.spid as session_id,
                sys2.blocked as blocking_session_id,
                sys2.lastwaittype,
                sys2.waittime,
                sys2.cpu,
                sys2.physical_io,
                sys2.memusage
            from
                sys.sysprocesses as sys1
                inner join
                sys.sysprocesses as sys2
                    on sys1.spid = sys2.blocked
            ) as blocked
        where
            (des.session_id = blocked.session_id or des.session_id = blocked.blocking_session_id)
    ) as blocked
    outer apply
    sys.dm_exec_sql_text(coalesce(der.sql_handle, blocked.sql_handle)) as dest
    outer apply
    sys.dm_exec_query_plan(der.plan_handle) as deqp
    left join
    sys.dm_exec_query_stats as deqs
        on der.sql_handle = deqs.sql_handle
        and der.plan_handle = deqs.plan_handle
        and der.statement_start_offset = deqs.statement_start_offset
        and der.statement_end_offset = deqs.statement_end_offset
where
    des.session_id <> @@spid
and
    des.host_name is not null
and
    coalesce(db_name(der.database_id), db_name(blocked.dbid)) is not null
order by
    der.total_elapsed_time desc;
