index_list.sql
==============

Generate a list of indexes within a database.

### Columns

Column                           | Description
-------------------------------- | --------------------------------------------
table_name                       | Table name with the index.
index_name                       | Index name.
column_name                      | Column name within the index.
index_column_id                  | Order of the columns within the index.
type_desc                        | Index type.
is_primary_key                   | Generated from a PRIMARY KEY (1=primary key).
is_unique                        | Index is unique (0=non unique, 1=unique).


### Required Permission

Query as database owner.
