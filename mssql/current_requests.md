current_requests.sql
====================

View requests (queries) currently executing.

### Columns

Column                           | Description
-------------------------------- | --------------------------------------------
time_now                         | Time this report is generated.
session_id                       | ID of the session which this request is related.
status                           | Request status, Background, Running, Runnable, Sleeping, Suspended.
blocking_session_id              | ID of the session that is blocking this request.
query_text                       | Text of the SQL query.
query_plan                       | Query plan in XML format.
total_elapsed_time_ms            | Total elapsed time in ms since the request arrived.
total_elapsed_time_sec           | Total elapsed time in seconds since the request arrived.
total_elapsed_time_min           | Total elapsed time in minutes since the request arrived.
cpu_time                         | CPU time used by this request.
reads                            | Number of disk reads performed by this request.
writes                           | Number of disk writes performed by this request.
logical_reads                    | Number of logical reads performed by this request.
host_name                        | Name of the client workstation.
login_name                       | SQL Server login used for this execution.
program_name                     | Client program that initiated the request.
db_name                          | Name of database the request is executing against.
request_id                       | Unique request ID.
transaction_id                   | ID of the transaction in which this request executes.
wait_type                        | Type of wait if this request is blocked.
wait_time                        | Wait time in ms if this request is blocked.
last_wait_type                   | Type of wait if this request has previously been blocked.
wait_resource                    | Resource the request is waiting for if blocked.
open_transaction_count           | Number of transaction open for this request.
open_resultset_count             | Number of result sets open for this request.
transaction_isolation_level      | Isolation level of the transaction for this request.
transaction_isolation_level_text | Isolation level of the transaction for this request in text form.
row_count                        | Number of rows returned to the client for this request.
objectid                         | ID of the object.
sql_handle                       | SQL handle for this request's SQL.
plan_handle                      | Plan handle for this requests's SQL execution plan.


### Required Permission

VIEW SERVER STATE
