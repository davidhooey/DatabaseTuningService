declare
    @PERF_LARGE_RAW_FRACTION int,
    @PERF_LARGE_RAW_BASE int;

select
    @PERF_LARGE_RAW_FRACTION = 537003264,
    @PERF_LARGE_RAW_BASE = 1073939712;

select
    dopc_fraction.object_name,
    dopc_fraction.instance_name,
    dopc_fraction.counter_name,
    cast(dopc_fraction.cntr_value as float) / cast(
        case dopc_base.cntr_value
        when 0 then
            null
        else
            dopc_base.cntr_value
        end as float
    ) as cntr_value
from
    sys.dm_os_performance_counters as dopc_base
    inner join
    sys.dm_os_performance_counters as dopc_fraction
        on dopc_base.cntr_type = @PERF_LARGE_RAW_BASE
        and dopc_fraction.cntr_type = @PERF_LARGE_RAW_FRACTION
        and dopc_base.object_name = dopc_fraction.object_name
        and dopc_base.instance_name = dopc_fraction.instance_name
        and (
            replace(dopc_base.counter_name, 'base', '') = dopc_fraction.counter_name
            or
            replace(dopc_base.counter_name, 'base', '') = (replace(dopc_fraction.counter_name, 'ratio', ''))
        )
order by
    dopc_fraction.object_name,
    dopc_fraction.instance_name,
    dopc_fraction.counter_name;
