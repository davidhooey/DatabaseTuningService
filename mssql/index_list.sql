use ApplicationDatabase;

select
    t.name table_name,
    i.name index_name,
    c.name column_name,
    ic.index_column_id,
    i.type_desc,
    i.is_primary_key,
    i.is_unique
from
    sys.indexes i
    inner join
    sys.index_columns ic
        on i.object_id = ic.object_id
        and i.index_id = ic.index_id
    inner join
    sys.columns c
        on ic.object_id = c.object_id
        and ic.column_id = c.column_id
    inner join
    sys.tables t
        on i.object_id = t.object_id
order by
    t.name,
    i.name,
    ic.index_column_id;
