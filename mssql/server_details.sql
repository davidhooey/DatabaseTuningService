select
    serverproperty('Edition') as sql_server_edition,
    serverproperty('ProductVersion') as sql_server_product_verison,
    serverproperty('ProductLevel') as sql_server_product_level,
    serverproperty('ServerName') as sql_server_server_name,
    sqlserver_start_time,
    cpu_count as logical_cpu_count,
    hyperthread_ratio,
    cpu_count / hyperthread_ratio as physical_cpu_count,
    physical_memory_kb,
    virtual_memory_kb,
    serverproperty('isClustered') as is_clustered
from
    sys.dm_os_sys_info