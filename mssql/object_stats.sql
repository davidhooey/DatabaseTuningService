use ApplicationDatabase;

select
    object_schema_name(s.object_id) object_schema,
    object_name(s.object_id) object_name,
    c.name column_name,
    s.name statistics_name,
    convert(varchar, sp.last_updated, 23) last_updated,
    sp.rows,
    sp.rows_sampled,
    sp.modification_counter,
    s.auto_created,
    s.user_created,
    s.no_recompute,
    sp.steps,
    sp.unfiltered_rows,
    s.has_filter,
    s.filter_definition
from
    sys.stats as s
    cross apply
        sys.dm_db_stats_properties(s.object_id, s.stats_id) as sp
    inner join
    sys.stats_columns as sc
        on s.object_id = sc.object_id
        and
        s.stats_id = sc.stats_id
    inner join
    sys.tables t
        on t.object_id = s.object_id
    inner join
    sys.schemas sch
        on sch.schema_id =  t.schema_id
    inner join
    sys.columns c
        on sc.object_id = c.object_id
        and sc.column_id = c.column_id
where
    sch.name <> 'sys'
order by
    object_name,
    column_name;
