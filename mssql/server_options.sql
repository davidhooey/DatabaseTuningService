select
    name,
    value,
    value_in_use,
    minimum,
    maximum,
    is_dynamic,
    is_advanced,
    description
from
    sys.configurations
order by
    name;
