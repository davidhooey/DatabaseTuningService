select r.* from (
select
    top_queries.snapshot_id,
    top_queries.snapshot_time,
    top_queries.sql_handle,
    top_queries.statement_start_offset,
    top_queries.statement_end_offset,
    top_queries.source_id,
    top_queries.execution_count,
    top_queries.total_elapsed_time,
    top_queries.avg_elapsed_time_per_exec,
    top_queries.min_elapsed_time,
    top_queries.max_elapsed_time,
    top_queries.total_cpu,
    top_queries.avg_cpu_per_exec,
    top_queries.total_logical_reads,
    top_queries.avg_logical_reads_per_exec,
    top_queries.total_physical_reads,
    top_queries.avg_physical_reads_per_exec,
    top_queries.total_logical_writes,
    top_queries.avg_logical_writes_per_exec,
    top_queries.row_count,
    top_queries.plan_count,
    stmt_text.database_id,
    stmt_text.object_id,
    stmt_text.object_name,
    replace(replace(stmt_text.query_text, char(13), ' '), char(10), ' ') as query_text,
    row_number() over (partition by top_queries.snapshot_id order by top_queries.avg_elapsed_time_per_exec desc) as rn
from
    (
        select
            qs.snapshot_id as snapshot_id,
            min(s.snapshot_time) as snapshot_time,
            qs.sql_handle as sql_handle,
            qs.statement_start_offset as statement_start_offset,
            qs.statement_end_offset as statement_end_offset,
            s.source_id,
            sum (qs.snapshot_execution_count) as execution_count,
            sum (qs.snapshot_elapsed_time / 1000) as total_elapsed_time,
            sum (qs.snapshot_elapsed_time / 1000.0) / case sum (qs.snapshot_execution_count) when 0 then 1 else sum (qs.snapshot_execution_count) end as avg_elapsed_time_per_exec,
            min (snapshot_elapsed_time) min_elapsed_time,
            max (snapshot_elapsed_time) max_elapsed_time,
            sum (qs.snapshot_worker_time / 1000) as total_cpu,
            sum (qs.snapshot_worker_time / 1000.0) / case sum (qs.snapshot_execution_count) when 0 then 1 else sum (qs.snapshot_execution_count) end as avg_cpu_per_exec,
            sum (qs.snapshot_logical_reads) as total_logical_reads,
            sum (qs.snapshot_logical_reads) / case sum (qs.snapshot_execution_count) when 0 then 1 else sum (qs.snapshot_execution_count) end as avg_logical_reads_per_exec,
            sum (qs.snapshot_physical_reads) as total_physical_reads,
            sum (qs.snapshot_physical_reads) / case sum (qs.snapshot_execution_count) when 0 then 1 else sum (qs.snapshot_execution_count) end as avg_physical_reads_per_exec,
            sum (qs.snapshot_logical_writes) as total_logical_writes,
            sum (qs.snapshot_logical_writes) / case sum (qs.snapshot_execution_count) when 0 then 1 else sum (qs.snapshot_execution_count) end as avg_logical_writes_per_exec,
            count(*) as row_count,
            count (distinct qs.creation_time) as plan_count
        from
            snapshots.query_stats qs
            inner join
            core.snapshots s
                on s.snapshot_id = qs.snapshot_id
        group by
            qs.snapshot_id,
            qs.sql_handle,
            qs.statement_start_offset,
            qs.statement_end_offset,
            s.source_id
    ) top_queries
    outer apply
        snapshots.fn_get_query_text (top_queries.source_id, top_queries.sql_handle, top_queries.statement_start_offset, top_queries.statement_end_offset) AS stmt_text
) r
where
    r.rn <= 50
order by
    snapshot_id desc,
    avg_elapsed_time_per_exec desc
