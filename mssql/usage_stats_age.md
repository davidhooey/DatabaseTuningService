usage_stats_age.sql
===================

Returns the number of days which historical statistics is available based on
the create date and time of the tempdb database. The creation of the tempdb
database indicates a service restart therefore the statistics in the dynamic
views was reset.


### Column Details

Column       | Description
------------ | ----------------------------------------------------------------
create_date  | The date and time the tempdb (service restart) was created.
days_history | The number of days of historical statisitcs available.


### Required Permission

VIEW ANY DATABASE
