locked_rows.sql
===============

Returns a list of rows from a specified table which are locked.


### Process

1. Use the `locking_blocking.sql` script first to determine which tables
   currently have locked rows.

2. Edit the `locked_rows.sql` script by replacing `table_name` with the table
   name with locked rows. For example.

    ```sql
    select
        t.*
    from
        my_table_name (nolock) as t
        join
        sys.dm_tran_locks as dtl
            on t.%%lockres%% = dtl.resource_description;
    ```

3. Run the query to deterine which rows are currently locked.
