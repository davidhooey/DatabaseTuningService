select
    convert(char, create_date, 107) create_date,
    datediff(day, create_date, getdate()) as days_history
from
    sys.databases
where
    name = 'tempdb';
