# Lock Escalation Tracking

We can use SQL Server Extended Events to track lock escalation events. Tracking lock escalation will help us determine
when each lock escalation occurs, on which object the lock escalation occurs, the SQL statement causing the lock
escalation, and the session ID causing the lock escalation.

1. Create an Extended Events session to track lock escalation. The following script will track lock escalation events
   and write the events to an event file.

    ```sql
    create event session [Track_Lock_Escalations]
    on server -- For SQL Server and Azure SQL Managed Instance
    /* on database -- For Azure SQL Database */
    add event sqlserver.lock_escalation
    (
        set
            collect_database_name=(0)
        action
        (
            sqlserver.client_app_name,
            sqlserver.client_connection_id,
            sqlserver.client_hostname,
            sqlserver.database_id,
            sqlserver.database_name,
            sqlserver.query_hash,
            sqlserver.query_plan_hash,
            sqlserver.server_instance_name,
            sqlserver.session_id,
            sqlserver.sql_text,
            sqlserver.username
        )
    )
    add target package0.event_file
    (
        set filename=N'C:\Temp\Track_Lock_Escalation', max_file_size=(250)
    )
    with
    (
        max_memory=4096 kb,
        event_retention_mode=allow_single_event_loss,
        max_dispatch_latency=30 seconds,
        max_event_size=0 kb,
        memory_partition_mode=none,
        track_causality=off,
        startup_state=off
    );
    ```
   
    _Note: specify the file path in the `filename` parameter._

2. Start the Extended Events session.

    ```sql
    alter event session [Track_Lock_Escalations]
    on server
    state = start;
    ```

    _Note: at this point lock escalation tracking is enabled._

3. Perform any tasks or test where lock escalation may occur.

4. Stop the Extended Events session.

    ```sql
    alter event session [Track_Lock_Escalations]
    on server
    state = stop;
    ```
   
   _Note: stop the session to flush all data to the tracking file._

5. Query the Extended Events XML session data in the tracking file to review the lock escalation events.

    ```sql
    select
        dateadd(mi, datediff(mi, getutcdate(), current_timestamp), event_data.value('(event/@timestamp)[1]', 'datetime2')) as [event_time],
        event_data.value('(event/action[@name="client_hostname"]/value)[1]', 'nvarchar(max)') as [client_hostname],
        event_data.value('(event/action[@name="client_app_name"]/value)[1]', 'nvarchar(max)') as [client_app_name],
        event_data.value('(event/action[@name="server_instance_name"]/value)[1]', 'nvarchar(max)') as [server_instance_name],
        event_data.value('(event/action[@name="username"]/value)[1]', 'nvarchar(max)') as [username],
        event_data.value('(event/action[@name="database_name"]/value)[1]', 'nvarchar(max)') as [database_name],
        event_data.value('(event/action[@name="session_id"]/value)[1]', 'int') as [session_id],
        event_data.value('(event/data[@name="object_id"]/value)[1]', 'int') as [object_id],
        object_name(
            event_data.value('(event/data[@name="object_id"]/value)[1]', 'int'),
            event_data.value('(event/action[@name="database_id"]/value)[1]', 'int')
        ) as [object_name],
        event_data.value('(event/data[@name="index_id"]/value)[1]', 'int') as [index_id],
        object_name(
            event_data.value('(event/data[@name="index_id"]/value)[1]', 'int'),
            event_data.value('(event/action[@name="database_id"]/value)[1]', 'int')
        ) as [index_name],
        event_data.value('(event/data[@name="resource_type"]/text)[1]', 'nvarchar(max)') as [resource_type],
        event_data.value('(event/data[@name="mode"]/text)[1]', 'nvarchar(max)') as [mode],
        event_data.value('(event/data[@name="escalation_cause"]/text)[1]', 'nvarchar(max)') as [escalation_cause],
        event_data.value('(event/data[@name="escalated_lock_count"]/value)[1]', 'int') as [escalated_lock_count],
        event_data.value('(event/action[@name="query_hash"]/value)[1]', 'nvarchar(max)') as [query_hash],
        event_data.value('(event/action[@name="query_plan_hash"]/value)[1]', 'nvarchar(max)') as [query_plan_hash],
        event_data.value('(event/action[@name="sql_text"]/value)[1]', 'nvarchar(max)') as [sql_text]
    from
    (
        select
            cast(event_data as xml) as event_data
        from
            sys.fn_xe_file_target_read_file(N'C:\Temp\*.xel', NULL, NULL, NULL)
    ) as EventData;
    ```

   _Note: specify the tracking file path location in the first argument of the fn_xe_file_target_read_file function._

6. The lock escalation tracker can remain in place for future use, or it can be dropped with the following statement.

    ```sql
    drop event session [Track_Lock_Escalations] on server;
    ```
