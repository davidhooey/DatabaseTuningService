locking_blocking_chain.sql
==========================

Display a tree like structure of the blocking chain. 

### Column Details

Column              | Description
------------------- | ---------------------------------------------------------
blocking_tree       | Blocking tree structure.
type                | A string indicating the name of the last or current wait type.
login_name          | Login name.
source_database     | Source database.
sql_text            | Last SQL statement.
cursor_sql_text     | SQL text of the batch that declared the cursor.
database            | Database associated to the locked resource.
schema              | Schema associated to the locked resource.
table               | Table associated to the locked resource.
wait_resource       | Textual representation of a lock resource.
command             | Command currently being executed.
application         | Name of the application program.
hostname            | Hostname of the client.
last_batch_time     | Last time a client process executed a statement.


### Required Permission

VIEW SERVER STATE
 