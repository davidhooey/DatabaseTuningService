Plan Guides
===========

Plan Guides can be used to force the usage of a particular query plan. A Plan
Guide can be created from a cached query plan, ShowPlan XML, or by specifying
OPTIONS to influence the optimizer.


### Creating Plan Guide From Cached Plan

Creating a Plan Guide from a cached query plan requires a `plan_guide_name`
and a cached `plan_handle`.

1. Determine `plan_handle` from the query cache matching the desired query
   plan. Use `cached_query_stats.sql` or `current_requests.sql` to determine
   the `plan_handle`. Also, the following script can be used to search for a
   cached query.
```sql
select
    deqs.plan_handle,
    dest.text,
    deqp.query_plan
from
    sys.dm_exec_query_stats deqs
    cross apply
    sys.dm_exec_sql_text(deqs.plan_handle) as dest
    cross apply
    sys.dm_exec_query_plan(deqs.plan_handle) as deqp    
where
    lower(dest.text) like '%query_search_string%';
```

2. Execute `sp_create_plan_guide_from_handle` specifying a `plan_guide_name` and
   passing in the `plan_handle` identified.
```sql
execute sp_create_plan_guide_from_handle
   @name =  N'plan_guide_name',
   @plan_handle = plan_handle
   @statement_start_offset = null;
```

3. Verify the Plan Guide was created.
```sql
select * from sys.plan_guides;
```


### Creating Plan Guide From Cached Plan Using Single Script

The following script can be used to create a Plan Guide by searching for a
cached query string. Change the `plan_guide_name` to the desired name, change
`query_search_string` to the query string to be found.

```sql
use ApplicationDatabase;
go

begin transaction

declare @plan_handle varbinary(64);

select
	@plan_handle = deqs.plan_handle
from
    sys.dm_exec_query_stats deqs
    cross apply
    sys.dm_exec_sql_text(deqs.plan_handle) as dest
where
    lower(dest.text) like '%query_search_string%';

execute sp_create_plan_guide_from_handle
    @name =  N'plan_guide_name',
    @plan_handle = @plan_handle,
    @statement_start_offset = null;

commit transaction
go

select * from sys.plan_guides;
go
```


### Creating Plan Guide From ShowPlan XML

Creating a Plan Guide using ShowPlan XML requires a `plan_guide_name`, the
`query_text` and the `showplan_xml`.

1. Execute `sp_create_plan_guide` specifying a `plan_guide_name`, `query_text` and
   `showplan_xml`.
```sql
execute sp_create_plan_guide
    @name = N'plan_guide_name',   
    @stmt = N'query_text',   
    @type = N'SQL',  
    @module_or_batch = NULL,   
    @params = NULL,   
    @hints = 'showplan_xml';  
```

2. Verify the Plan Guide was created.
```sql
select * from sys.plan_guides;
```


### Creating Plan Guide Using OPTIONS

Creating a Plan Guide using OPTIONS requires a `plan_guide_name`, the
`query_text` and `options`.

1. Execute `sp_create_plan_guide` specifying a `plan_guide_name`, `query_text` and
   `showplan_xml`.

Syntax:
```sql
execute sp_create_plan_guide
    @name = N'plan_guide_name',
    @stmt = N'query_text',
    @type = N'SQL',  
    @module_or_batch = NULL,   
    @params = NULL,   
    @hints = N'OPTION (options)';
```

Example: Set MAXDOP to 4.
```sql
execute sp_create_plan_guide
    @name = N'plan_guide_name',
    @stmt = N'query_text',
    @type = N'SQL',  
    @module_or_batch = NULL,   
    @params = NULL,   
    @hints = N'OPTION (MAXDOP 4)';  
```

2. Verify the Plan Guide was created.
```sql
select * from sys.plan_guides;
```


### Verify Plan Guide Is Used

After the Plan Guide has been created, subsequent executions of the query
should use the query plan within the Plan Guide. Using `cached_query_stats.sql`
or `current_requests.sql` scripts, will show the query plan.

Verify using query plan XML:

- The XML of the query plan should contain the `PlanGuideDB="DatabaseName"` and
  `PlanGuideName="Plan Guide Name"` attributes. These attributes indicate the
  Plan Guide is being used.

Verify using the query plan diagram:

- Right-click on the first node in the diagram and select Properties. The
  properties should show the `PlanGuideDB` and `PlanGuideName` properties.


### Deleting Plan Guide

To delete a Plan Guide, supply the Plan Guide name to the following procedure.

```sql
execute sp_control_plan_guide N'DROP', N'Plan Guide Name To Delete';
```
