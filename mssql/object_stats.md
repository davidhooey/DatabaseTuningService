object_stats.sql
================

Show table and index statistics information.

### Column Details

Column               | Description
-------------------- | --------------------------------------------------------------------------------------
schema_name          | Schema name.
object_name          | Object name.
column_name          | Column name.
statistics_name      | Name of the statistic. Unique to each object.
late_updated         | Date of last statistics update.
rows                 | Total number of rows in the table or indexed since last update.
rows_sampled         | Total number of rows sampled for statistics calculations.
modification_counter | Total number of modifications for the leading statistics column since last update.
auto_created         | Statistics were automatically created by SQL Server (0=No, 1=Yes).
user_created         | Statistics were created by a user (0=No, 1=Yes).
steps                | Number of steps in the histogram.
unfiltered_rows      | Total number of rows in the table before applying the filter expression.
no_recompute         | Statistics were created with the NORECOMPUTE option (0=No, 1=Yes).
has_filter           | 0 = Computed on all rows, 1 = Computed on filtered rows only.
filter_definition    | Filter statistics express, NULL = no filter.


### Required Permission

Query as database owner.
