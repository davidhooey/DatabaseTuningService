select
    dtl.resource_type,
    db_name(dtl.resource_database_id) as database_name,
    case
    when dtl.resource_type in ('DATABASE', 'FILE', 'METADATA') then
        dtl.resource_type
    when dtl.resource_type = 'OBJECT' then
        object_name(dtl.resource_associated_entity_id)
    when dtl.resource_type in ('KEY', 'PAGE', 'RID') then
        (select object_name(object_id) from sys.partitions where sys.partitions.hobt_id = dtl.resource_associated_entity_id)
    else
        'Unidentified'
    end as requested_object_name,
    dtl.resource_associated_entity_id,
    dtl.request_mode,
    dtl.request_status,
    dowt.wait_duration_ms,
    dowt.wait_type,
    dowt.session_id as blocked_session_id,
    des_blocked.login_name as blocked_user,
    dest_blocked.text as blocked_command,
    dowt.blocking_session_id,
    des_blocking.login_name as blocking_user,
    dest_blocking.text as blocking_command,
    dowt.resource_description
from
    sys.dm_tran_locks dtl
    inner join
    sys.dm_os_waiting_tasks dowt
        on dtl.request_session_id = dowt.blocking_session_id
    inner join
    sys.dm_exec_requests der
        on dowt.session_id = der.session_id
    inner join
    sys.dm_exec_sessions des_blocked
        on dowt.session_id = des_blocked.session_id
    inner join
    sys.dm_exec_sessions des_blocking
        on dowt.blocking_session_id = des_blocking.session_id
    inner join
    sys.dm_exec_connections dec
        on dtl.request_session_id = dec.most_recent_session_id
    cross apply
    sys.dm_exec_sql_text(dec.most_recent_sql_handle) as dest_blocking
    cross apply
    sys.dm_exec_sql_text(der.sql_handle) as dest_blocked
where
    db_name(dtl.resource_database_id) = 'ApplicationDatabase'
and
    dtl.resource_associated_entity_id > 0
and
    dtl.resource_type <> 'DATABASE';