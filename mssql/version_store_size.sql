select
    db_name(database_id) database_name,
    sum(record_length_first_part_in_bytes + record_length_second_part_in_bytes) total_bytes
from
    sys.dm_tran_version_store
group by
    db_name(database_id)
order by
    database_name;
