server_details.sql
==================

SQL Server and server details.


### Column Details

Column                     | Description
-------------------------- | --------------------------------------------------
sql_server_edition         | SQL Server edition.
sql_server_product_verison | SQL Server instance version
sql_server_product_level   | SQL Server level RTM (shipping version), SPn (service pack verison), Bn (beta version).
sql_server_server_name     | Windows server and SQL Server instance name.
sqlserver_start_time       | Date and time SQL Server last started.
logical_cpu_count          | Number of logical CPUs.
hyperthread_ratio          | Ratio of the number of logical or physical cores.
physical_cpu_count         | Number of physical CPUs.
physical_memory_kb         | Total amount of physical memory.
virtual_memory_kb          | Total amount of virtual address space available.
is_clustered               | Is the server instance configured in a failover cluster 0=No, 1=Yes.


### Required Permission

VIEW SERVER STATE
