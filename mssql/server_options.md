server_options.sql
==================

Contains a row for each server-wide configuration option value in the system.


### Column Details

Column       | Description
------------ | -------------------------------------------------------
name         | Name of the configuration option.
value        | Configured value for this option.
value_in_use | Running value currently in effect for this option.
minimum      | Minimum value for the configuration option.
maximum      | Maximum value for the configuration option.
is_dynamic   | 1 = The variable that takes effect when the RECONFIGURE statement is executed.
is_advanced  | 1 = The variable is displayed only when the show advancedoption is set.
description  | Description of the configuration option.


### Required Permission

Requires membership in the `public` role.
