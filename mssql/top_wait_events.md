top_wait_events.sql
===================

Whenever SQL Server is waiting on a resource, it records the time spend
waiting. Time is accumulated since SQL Server startup.

### Column Details

Column                  | Description
----------------------- | ---------------------------------------------------------------
server_utc_startup_time | Server startup UTC time.
sample_utc_time         | UTC time the sample was collected.
sample_time             | Time the sample was collected.
hours_sample            | Hours since server startup.
wait_type               | The name of the wait event.
wait_time_hours         | Accumulated hours waiting for this event.
per_core_per_hour       | Accumulated hours waiting per CPU core for this event.
signal_wait_time_hours  | Amount of time any request waited after being signaled that the resource was freed. A high signal wait time is indicative of CPU issues in that the thread still had to wait for CPU assignment even after the resource being waited on was freed up.
percent_signal_waits    | Percentage of signal waits.
number_of_waits         | Cumulative number of tasks that have registered this wait.
avg_ms_per_wait         | Average ms per wait.
url                     | Link for more information regardingt his wait event.

### Required Permission

VIEW SERVER STATE
