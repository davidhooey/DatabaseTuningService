select top 100
    /* Database */
    dest.dbid,
    d.name,
    /* SQL */
    deqs.sql_handle,
	substring(
		dest.text,
		(deqs.statement_start_offset/2)+1,
		((
			case deqs.statement_end_offset
			when -1 then
				datalength(dest.text)
			else
				deqs.statement_end_offset
			end - deqs.statement_start_offset
		)/2) + 1
	) as query_text,
    /* Plan */
    deqs.plan_handle,
    deqp.query_plan,
    /* Execution Details */
    deqs.execution_count,
    deqs.creation_time,
    deqs.last_execution_time,
    /* Execution Times */
    deqs.total_elapsed_time as total_elapsed_time_us,
	deqs.total_elapsed_time / 1000 as total_elapsed_time_ms,
	deqs.total_elapsed_time / 1000000 as total_elapsed_time_sec,
	deqs.total_elapsed_time / 1000000 / 60 as total_elapsed_time_min,
    deqs.total_elapsed_time / deqs.execution_count as average_elapsed_time_us,
	deqs.total_elapsed_time / deqs.execution_count / 1000 as average_elapsed_time_ms,
	deqs.total_elapsed_time / deqs.execution_count / 1000000 as average_elapsed_time_sec,
	deqs.total_elapsed_time / deqs.execution_count / 1000000 / 60 as average_elapsed_time_min,
    deqs.last_elapsed_time as last_elapsed_time_us,
	deqs.last_elapsed_time / 1000 as last_elapsed_time_ms,
	deqs.last_elapsed_time / 1000000 as last_elapsed_time_sec,
	deqs.last_elapsed_time / 1000000 / 60 as last_elapsed_time_min,
    deqs.min_elapsed_time as min_elapsed_time_us,
	deqs.min_elapsed_time / 1000 as min_elapsed_time_ms,
	deqs.min_elapsed_time / 1000000 as min_elapsed_time_sec,
	deqs.min_elapsed_time / 1000000 / 60 as min_elapsed_time_min,
    deqs.max_elapsed_time as max_elapsed_time_us,
	deqs.max_elapsed_time / 1000 as max_elapsed_time_ms,
	deqs.max_elapsed_time / 1000000 as max_elapsed_time_sec,
	deqs.max_elapsed_time / 1000000 / 60 as max_elapsed_time_min,
    /* Rows */
    deqs.total_rows,
    deqs.total_rows / deqs.execution_count as average_rows,
    deqs.last_rows,
    deqs.min_rows,
    deqs.max_rows,
    /* CPU Time */
    deqs.total_worker_time,
    deqs.total_worker_time / deqs.execution_count as average_worker_time,
    deqs.last_worker_time,
    deqs.min_worker_time,
    deqs.max_worker_time,
    /* Physical Reads */
    deqs.total_physical_reads,
    deqs.total_physical_reads / deqs.execution_count as average_physical_reads,
    deqs.last_physical_reads,
    deqs.min_physical_reads,
    deqs.max_physical_reads,
    /* Logical Reads */
    deqs.total_logical_reads,
    deqs.total_logical_reads / deqs.execution_count as average_logical_reads,
    deqs.last_logical_reads,
    deqs.min_logical_reads,
    deqs.max_logical_reads,
    /* Logical Writes */
    deqs.total_logical_writes,
    deqs.total_logical_writes / deqs.execution_count as average_logical_writes,
    deqs.last_logical_writes,
    deqs.min_logical_writes,
    deqs.max_logical_writes,
    /* Cache Removal */
	case
    when deqs.sql_handle is not null then
		'dbcc freeproccache(' + convert(varchar(128), deqs.sql_handle, 1) + ');'
	else
        'N/A'
    end as remove_sql_handle_from_cache,
	case
    when deqs.plan_handle is not null then
		'dbcc freeproccache(' + convert(varchar(128), deqs.plan_handle, 1) + ');'
	else
        'N/A'
    end as remove_plan_handle_from_cache
from
    sys.dm_exec_query_stats deqs
    cross apply
    sys.dm_exec_sql_text(deqs.plan_handle) as dest
    cross apply
    sys.dm_exec_query_plan(deqs.plan_handle) as deqp
    inner join
    sys.databases d
        on dest.dbid = d.database_id
where
    d.name = 'ApplicationDatabase'
order by
    /* By Total Elapsed Time */
    /* total_elapsed_time_us desc; */
    /* By Average Elapsed Time */
    average_elapsed_time_us desc;
    /* By Max Elapsed Time */
    /* max_elapsed_time_us desc; */
    /* By CPU Time */
    /* deqs.total_worker_time desc; */
    /* By Physical Reads */
    /* deqs.total_physical_reads desc; */
