select
    des.program_name,
    des.login_name,
    des.host_name,
    dec.client_net_address,
    des.session_id,
    des.database_id, -- SQL Server 2012
    des.status,
    des.cpu_time,
    des.memory_usage,
    des.reads,
    des.writes,
    des.logical_reads,
    des.row_count,
    des.total_scheduled_time,
    des.total_elapsed_time,
    des.last_request_start_time,
    des.last_request_end_time,
    des.transaction_isolation_level,
    case des.transaction_isolation_level
        when 0 then 'Unspecified'
        when 1 then 'ReadUncommitted'
        when 2 then 'ReadCommitted'
        when 3 then 'Repeatable'
        when 4 then 'Serializable'
        when 5 then 'Snapshot'
    end as transaction_isolation_level_text,
    des.open_transaction_count -- SQL Server 2012
from
    sys.dm_exec_sessions as des
    inner join
    sys.dm_exec_connections as dec
        on des.session_id = dec.session_id
where
    des.program_name = 'Content Server Core C++ Components'
go
