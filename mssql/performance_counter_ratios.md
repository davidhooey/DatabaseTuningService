performance_counter_ratios.sql
==============================

Uses the sys.dm_os_performance_counters view containing the performance
counter information typically seen through Performance Monitor. Only the
ratio counters are calculated and displayed.


### Column Details

Column        | Description
------------- | ---------------------------------------------------------------
object_name   | Category to which the counter belongs.
instance_name | Name of the specific instance of the counter. Often contains the database name.
counter_name  | Name of the counter.
cntr_value    | Calculated ratio.


### Required Permission

VIEW SERVER STATE
