/*
    Compare Two First Responder Kit Check Dates

    Description:
        This script will compare two sets of First ResResponder Kit data based on the CheckDate field.

    Setup:
        Set the @FirstTime and @SecondTime variables to the two CheckDate values you want to compare.
        Set the @FirstName and @SecondName variables to the names you want to use for the two sets of data.
 */

declare @FirstName varchar(25) = 'slow';
declare @FirstTime datetimeoffset = '2025-02-19 03:30:03.2056990 -06:00';

declare @SecondName varchar(25) = 'fast';
declare @SecondTime datetimeoffset = '2025-02-19 14:40:04.2079642 -06:00';

print '==== Blitz (' + @FirstName + ') ====';
select 'Blitz_' + @FirstName as Report, *
from BlitzFirst
where CheckDate = @FirstTime
order by id;

print '==== Blitz (' + @SecondName + ') ====';
select 'Blitz_' + @SecondName as Report, *
from BlitzFirst
where CheckDate = @SecondTime
order by id;

print '==== BlitzFirst_FileStats (' + @FirstName + ') ====';
select
    'BlitzFirst_FileStats_' + @FirstName  as Report,
    ServerName,
    DatabaseName,
    FileLogicalName,
    PhysicalName,
    TypeDesc,
    SizeOnDiskMB,
    io_stall_read_ms,
    num_of_reads,
    io_stall_read_ms/num_of_reads as avg_read_ms,
    bytes_read,
    io_stall_write_ms,
    num_of_writes,
    io_stall_write_ms/num_of_writes as avg_write_ms,
    bytes_written
from
    BlitzFirst_FileStats
where
    CheckDate = @FirstTime;

print '==== BlitzFirst_FileStats (' + @SecondName + ') ====';
select
    'BlitzFirst_FileStats_' + @SecondName  as Report,
    ServerName,
    DatabaseName,
    FileLogicalName,
    PhysicalName,
    TypeDesc,
    SizeOnDiskMB,
    io_stall_read_ms,
    num_of_reads,
    io_stall_read_ms/num_of_reads as avg_read_ms,
    bytes_read,
    io_stall_write_ms,
    num_of_writes,
    io_stall_write_ms/num_of_writes as avg_write_ms,
    bytes_written
from
    BlitzFirst_FileStats
where
    CheckDate = @SecondTime;

print '==== FileStats Comparison ====';

declare @file_stats_comp nvarchar(max);

set @file_stats_comp = '
with
first_set as
(
    select
        ''BlitzFirst_FileStats'' as Report,
        ServerName,
        DatabaseName,
        FileLogicalName,
        PhysicalName,
        TypeDesc,
        SizeOnDiskMB,
        io_stall_read_ms,
        num_of_reads,
        io_stall_read_ms/num_of_reads as avg_read_ms,
        bytes_read,
        io_stall_write_ms,
        num_of_writes,
        io_stall_write_ms/num_of_writes as avg_write_ms,
        bytes_written
    from
        BlitzFirst_FileStats
    where
        CheckDate = @FirstTime
),
second_set as
(
    select
        ''BlitzFirst_FileStats'' as Report,
        ServerName,
        DatabaseName,
        FileLogicalName,
        PhysicalName,
        TypeDesc,
        SizeOnDiskMB,
        io_stall_read_ms,
        num_of_reads,
        io_stall_read_ms/num_of_reads as avg_read_ms,
        bytes_read,
        io_stall_write_ms,
        num_of_writes,
        io_stall_write_ms/num_of_writes as avg_write_ms,
        bytes_written
    from
        BlitzFirst_FileStats
    where
        CheckDate = @SecondTime
)
select
    o.Report as Report,
    o.ServerName as ServerName,
    o.DatabaseName as DatabaseName,
    o.FileLogicalName as FileLogicalName,
    o.PhysicalName as PhysicalName,
    o.TypeDesc as TypeDesc,
    o.SizeOnDiskMB as ' + quotename(@FirstName + '_SizeOnDiskMB') + ',
    t.SizeOnDiskMB as ' + quotename(@SecondName + '_SizeOnDiskMB') + ',
    o.io_stall_read_ms as ' + quotename(@FirstName + '_io_read_ms') + ',
    t.io_stall_read_ms as ' + quotename(@SecondName + '_io_read_ms') + ',
    o.num_of_reads as ' + quotename(@FirstName + '_num_reads') + ',
    t.num_of_reads as ' + quotename(@SecondName + '_num_reads') + ',
    o.avg_read_ms as ' + quotename(@FirstName + '_avg_read_ms') + ',
    t.avg_read_ms as ' + quotename(@SecondName + '_avg_read_ms') + ',
    o.bytes_read as ' + quotename(@FirstName + '_bytes_read') + ',
    t.bytes_read as ' + quotename(@SecondName + '_bytes_read') + ',
    o.io_stall_write_ms as ' + quotename(@FirstName + '_io_write_ms') + ',
    t.io_stall_write_ms as ' + quotename(@SecondName + '_io_write_ms') + ',
    o.num_of_writes as ' + quotename(@FirstName + '_num_writes') + ',
    t.num_of_writes as ' + quotename(@SecondName + '_num_writes') + ',
    o.avg_write_ms as ' + quotename(@FirstName + '_avg_write_ms') + ',
    t.avg_write_ms as ' + quotename(@SecondName + '_avg_write_ms') + ',
    o.bytes_written as ' + quotename(@FirstName + '_bytes_written') + ',
    t.bytes_written as ' + quotename(@SecondName + '_bytes_written') + '
from
    first_set o
    inner join
    second_set t
        on o.ServerName = t.ServerName
        and o.DatabaseName = t.DatabaseName
        and o.FileLogicalName = t.FileLogicalName
        and o.PhysicalName = t.PhysicalName
        and o.TypeDesc = t.TypeDesc
order by
    ServerName,
    DatabaseName,
    FileLogicalName,
    PhysicalName,
    TypeDesc;
'

exec sp_executesql @file_stats_comp, N'@FirstTime datetimeoffset, @SecondTime datetimeoffset', @FirstTime, @SecondTime;

print '==== BlitzFirst_PerfmonStats (' + @FirstName + ') ====';
select
    'BlitzFirst_PerfmonStats_' + @FirstName as Report,
    object_name,
    counter_name,
    instance_name,
    value_delta,
    value_per_second
from
    BlitzFirst_PerfmonStats
where
    CheckDate = @FirstTime;

print '==== BlitzFirst_PerfmonStats (' + @SecondName + ') ====';
select
    'BlitzFirst_PerfmonStats_' + @SecondName as Report,
    object_name,
    counter_name,
    instance_name,
    value_delta,
    value_per_second
from
    BlitzFirst_PerfmonStats
where
    CheckDate = @SecondTime;

print '==== PerfmonStats Comparison ====';

declare @perf_stats_comp nvarchar(max);

set @perf_stats_comp = '
with
first_set as
(
    select
        ''BlitzFirst_PerfmonStats'' as Report,
        object_name,
        counter_name,
        instance_name,
        value_delta,
        value_per_second
    from
        BlitzFirst_PerfmonStats
    where
        CheckDate = @FirstTime
),
second_set as
(
    select
        ''BlitzFirst_PerfmonStats'' as Report,
        object_name,
        counter_name,
        instance_name,
        value_delta,
        value_per_second
    from
        BlitzFirst_PerfmonStats
    where
        CheckDate = @SecondTime
)
select
    o.Report as Report,
    o.object_name as object_name,
    o.counter_name as counter_name,
    o.instance_name as instance_name,
    o.value_delta as ' + quotename(@FirstName + '_value_delta') + ',
    t.value_delta as  ' + quotename(@SecondName + '_value_delta') + ',
    o.value_per_second as ' + quotename(@FirstName + '_value_per_second') + ',
    t.value_per_second as  ' + quotename(@SecondName + '_value_per_second') + ',
    o.value_per_second - t.value_per_second as value_per_second_diff
from
    first_set o
    inner join
    second_set t
        on o.object_name = t.object_name
        and o.counter_name = t.counter_name
        and o.instance_name = t.instance_name
order by
    object_name,
    counter_name,
    instance_name;
';

exec sp_executesql @perf_stats_comp, N'@FirstTime datetimeoffset, @SecondTime datetimeoffset', @FirstTime, @SecondTime;

-- TODO: Dynamic SQL for BlitzFirst_WaitStats

print '==== BlitzFirst_WaitStats (' + @FirstName + ') ====';
select
    'BlitzFirst_WaitStats_' + @FirstName as Report,
    ServerName,
    wait_type,
    wait_time_ms,
    signal_wait_time_ms,
    waiting_tasks_count
from
    BlitzFirst_WaitStats
where
    CheckDate = @FirstTime

print '==== BlitzFirst_WaitStats (' + @SecondName + ') ====';
select
    'BlitzFirst_WaitStats_' + @SecondName as Report,
    ServerName,
    wait_type,
    wait_time_ms,
    signal_wait_time_ms,
    waiting_tasks_count
from
    BlitzFirst_WaitStats
where
    CheckDate = @SecondTime

print '==== WaitStats Comparison ====';

declare @wait_stats_comp nvarchar(max);

set @wait_stats_comp = '
with
first_set as
(
    select
        ''BlitzFirst_WaitStats'' as Report,
        ServerName,
        wait_type,
        wait_time_ms,
        signal_wait_time_ms,
        waiting_tasks_count
    from
        BlitzFirst_WaitStats
    where
        CheckDate = @FirstTime
),
second_set as
(
    select
        ''BlitzFirst_WaitStats'' as Report,
        ServerName,
        wait_type,
        wait_time_ms,
        signal_wait_time_ms,
        waiting_tasks_count
    from
        BlitzFirst_WaitStats
    where
        CheckDate = @SecondTime
)
select
    o.Report as Report,
    o.ServerName as ServerName,
    o.wait_type as wait_type,
    o.wait_time_ms as ' + quotename(@FirstName + '_wait_time_ms') + ',
    t.wait_time_ms as ' + quotename(@SecondName + '_wait_time_ms') + ',
    o.wait_time_ms - t.wait_time_ms as wait_time_ms_diff,
    o.signal_wait_time_ms as ' + quotename(@FirstName + '_signal_wait_time_ms') + ',
    t.signal_wait_time_ms as ' + quotename(@SecondName + '_signal_wait_time_ms') + ',
    o.signal_wait_time_ms - t.signal_wait_time_ms as signal_wait_time_ms_diff,
    o.waiting_tasks_count as ' + quotename(@FirstName + '_waiting_tasks_count') + ',
    t.waiting_tasks_count as ' + quotename(@SecondName + '_waiting_tasks_count') + ',
    o.waiting_tasks_count - t.waiting_tasks_count as waiting_tasks_count_diff
from
    first_set o
    inner join
    second_set t
        on o.ServerName = t.ServerName
        and o.wait_type = t.wait_type
order by
    ' + quotename(@FirstName + '_wait_time_ms') + ' desc
'

exec sp_executesql @wait_stats_comp, N'@FirstTime datetimeoffset, @SecondTime datetimeoffset', @FirstTime, @SecondTime;

PRINT '==== Who ' + @FirstName + ' ====';
select
    'BlitzWho ' + @FirstName as Report, CheckDate, ServerName, elapsed_time, session_id, database_name, query_text,
    cached_parameter_info, live_parameter_info, query_cost, status, wait_info, wait_resource, top_session_waits,
    blocking_session_id, open_transaction_count, is_implicit_transaction, nt_domain, host_name, login_name,
    nt_user_name, program_name, fix_parameter_sniffing, client_interface_name, login_time, start_time, request_time,
    request_cpu_time, request_logical_reads, request_writes, request_physical_reads, session_cpu, session_logical_reads,
    session_physical_reads, session_writes, tempdb_allocations_mb, memory_usage, estimated_completion_time,
    percent_complete, deadlock_priority, transaction_isolation_level, degree_of_parallelism, last_dop, min_dop, max_dop,
    last_grant_kb, min_grant_kb, max_grant_kb, last_used_grant_kb, min_used_grant_kb, max_used_grant_kb,
    last_ideal_grant_kb, min_ideal_grant_kb, max_ideal_grant_kb, last_reserved_threads, min_reserved_threads,
    max_reserved_threads, last_used_threads, min_used_threads, max_used_threads, grant_time, requested_memory_kb,
    grant_memory_kb, is_request_granted, required_memory_kb, query_memory_grant_used_memory_kb, ideal_memory_kb,
    is_small, timeout_sec, resource_semaphore_id, wait_order, wait_time_ms, next_candidate_for_memory_grant,
    target_memory_kb, max_target_memory_kb, total_memory_kb, available_memory_kb, granted_memory_kb,
    query_resource_semaphore_used_memory_kb, grantee_count, waiter_count, timeout_error_count, forced_grant_count,
    workload_group_name, resource_pool_name, context_info, query_hash, query_plan_hash, sql_handle, plan_handle,
    statement_start_offset, statement_end_offset, JoinKey
from
    BlitzWho
where
    CheckDate = @FirstTime
order by
    elapsed_time desc;

PRINT '==== Who ' + @SecondName + ' ====';
select
    'BlitzWho ' + @SecondName as Report, CheckDate, ServerName, elapsed_time, session_id, database_name, query_text,
    cached_parameter_info, live_parameter_info, query_cost, status, wait_info, wait_resource, top_session_waits,
    blocking_session_id, open_transaction_count, is_implicit_transaction, nt_domain, host_name, login_name,
    nt_user_name, program_name, fix_parameter_sniffing, client_interface_name, login_time, start_time, request_time,
    request_cpu_time, request_logical_reads, request_writes, request_physical_reads, session_cpu, session_logical_reads,
    session_physical_reads, session_writes, tempdb_allocations_mb, memory_usage, estimated_completion_time,
    percent_complete, deadlock_priority, transaction_isolation_level, degree_of_parallelism, last_dop, min_dop, max_dop,
    last_grant_kb, min_grant_kb, max_grant_kb, last_used_grant_kb, min_used_grant_kb, max_used_grant_kb,
    last_ideal_grant_kb, min_ideal_grant_kb, max_ideal_grant_kb, last_reserved_threads, min_reserved_threads,
    max_reserved_threads, last_used_threads, min_used_threads, max_used_threads, grant_time, requested_memory_kb,
    grant_memory_kb, is_request_granted, required_memory_kb, query_memory_grant_used_memory_kb, ideal_memory_kb,
    is_small, timeout_sec, resource_semaphore_id, wait_order, wait_time_ms, next_candidate_for_memory_grant,
    target_memory_kb, max_target_memory_kb, total_memory_kb, available_memory_kb, granted_memory_kb,
    query_resource_semaphore_used_memory_kb, grantee_count, waiter_count, timeout_error_count, forced_grant_count,
    workload_group_name, resource_pool_name, context_info, query_hash, query_plan_hash, sql_handle, plan_handle,
    statement_start_offset, statement_end_offset, JoinKey
from
    BlitzWho
where
    CheckDate = @SecondTime
order by
    elapsed_time desc;
