# First Responder Kit

The SQL Server First Responder Kit is a set of open source stored procedures to
help troubleshoot SQL Server performance issues. The stored procedures can be
scheduled to run and store performance data in to a set of tables.

## Installation

1. Go to the GitHub repository and select the latest version tag to ensure you
   are viewing the most recent release.

    [https://github.com/BrentOzarULTD/SQL-Server-First-Responder-Kit/releases](https://github.com/BrentOzarULTD/SQL-Server-First-Responder-Kit/releases)

2. Copy the contents of the `Install-All-Scripts.sql` script.

3. In SQL Server Management Studio (SSMS) paste the contents of the script into
   a new query window and run the script in the `master` database. This will
   create a bunch of `sp_Blitz*` stored procedures.

4. At this point the First Responder Kit is installed and ready to use. For
   example, to determine why SQL Server is slow right now, the following will
   return the top queries, top wait events and perfmon counters.

    ```sql
    execute sp_BlitzFirst @ExpertMode = 1;
    ```

## Collect Performance Data

1. Create a new database named `FirstResponderKit`.

2. Create a SQL Server Agent job that runs the following statement every 10
   minutes. Set the `@OutputTableRetentionDays` to the number of days of
   data to keep.

    ```sql
    execute sp_BlitzFirst
        @OutputDatabaseName = 'FirstResponderKit',
        @OutputSchemaName = 'dbo',
        @OutputTableName = 'BlitzFirst',
        @OutputTableNameFileStats = 'BlitzFirst_FileStats',
        @OutputTableNamePerfmonStats = 'BlitzFirst_PerfmonStats',
        @OutputTableNameWaitStats = 'BlitzFirst_WaitStats',
        @OutputTableNameBlitzCache = 'BlitzCache',
        @OutputTableNameBlitzWho = 'BlitzWho',
        @OutputTableRetentionDays = 14,
        @ExpertMode = 1,
        @Seconds = 10;
    ```

3. As the stored procedure is run every 10 mins, it will log the data into
   tables in the FirstResponderKit database and retain the data for the
   specified number of days.

## Analyzing Performance Data

Determine a time frame to analyze and update the `@StartDate` and `@EndDate` parameters. Update the `@Output*` parameters
to point to the database, schema and table names holding the collected data. Run the procedure.

```sql
exec sp_BlitzAnalysis
    @StartDate = '2025-02-18 17:00:00',
    @EndDate = '2025-02-18 19:00:00',
    @OutputDatabaseName = 'FirstResponderKit',
    @OutputSchemaName = 'dbo',
    @OutputTableNameFileStats = N'BlitzFirst_FileStats',
    @OutputTableNamePerfmonStats  = N'BlitzFirst_PerfmonStats',
    @OutputTableNameWaitStats = N'BlitzFirst_WaitStats',
    @OutputTableNameBlitzCache = N'BlitzCache',
    @OutputTableNameBlitzWho = N'BlitzWho';
```
