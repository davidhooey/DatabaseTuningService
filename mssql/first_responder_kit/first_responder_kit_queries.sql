-- Get a list of CheckDates dates.
select CheckDate
from dbo.BlitzCache
order by CheckDate desc;

-- BlitzFirst
select
    ID,
    ServerName,
    CheckDate,
    CheckID,
    Priority,
    FindingsGroup,
    Finding,
    URL,
    Details,
    QueryText,
    StartTime,
    LoginName,
    NTUserName,
    OriginalLoginName,
    ProgramName,
    HostName,
    DatabaseID,
    DatabaseName,
    OpenTransactionCount,
    DetailsInt,
    QueryHash,
    JoinKey
from
    dbo.BlitzFirst
where
    CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by
    Priority;

-- BlitzFirst_WaitStats
select *
from dbo.BlitzFirst_WaitStats_Deltas
where CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by wait_time_ms_delta desc;

-- BlitzWho
select
    ID,
    ServerName,
    CheckDate,
    elapsed_time,
    session_id,
    database_name,
    query_text_snippet,
    query_cost,
    status,
    wait_info,
    top_session_waits,
    blocking_session_id,
    open_transaction_count,
    is_implicit_transaction,
    nt_domain,
    host_name,
    login_name,
    nt_user_name,
    program_name,
    fix_parameter_sniffing,
    client_interface_name,
    login_time,
    start_time,
    request_time,
    request_cpu_time,
    degree_of_parallelism,
    request_logical_reads,
    Logical_Reads_MB,
    request_writes,
    Logical_Writes_MB,
    request_physical_reads,
    Physical_reads_MB,
    session_cpu,
    session_logical_reads,
    session_logical_reads_MB,
    session_physical_reads,
    session_physical_reads_MB,
    session_writes,
    session_writes_MB,
    tempdb_allocations_mb,
    memory_usage,
    estimated_completion_time,
    percent_complete,
    deadlock_priority,
    transaction_isolation_level,
    last_dop,
    min_dop,
    max_dop,
    last_grant_kb,
    min_grant_kb,
    max_grant_kb,
    last_used_grant_kb,
    min_used_grant_kb,
    max_used_grant_kb,
    last_ideal_grant_kb,
    min_ideal_grant_kb,
    max_ideal_grant_kb,
    last_reserved_threads,
    min_reserved_threads,
    max_reserved_threads,
    last_used_threads,
    min_used_threads,
    max_used_threads,
    grant_time,
    requested_memory_kb,
    grant_memory_kb,
    is_request_granted,
    required_memory_kb,
    query_memory_grant_used_memory_kb,
    ideal_memory_kb,
    is_small,
    timeout_sec,
    resource_semaphore_id,
    wait_order,
    wait_time_ms,
    next_candidate_for_memory_grant,
    target_memory_kb,
    max_target_memory_kb,
    total_memory_kb,
    available_memory_kb,
    granted_memory_kb,
    query_resource_semaphore_used_memory_kb,
    grantee_count,
    waiter_count,
    timeout_error_count,
    forced_grant_count,
    workload_group_name,
    resource_pool_name,
    context_info,
    query_hash,
    query_plan_hash,
    sql_handle,
    plan_handle,
    statement_start_offset,
    statement_end_offset
from
    dbo.BlitzWho_Deltas
where
    CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by
    elapsed_time desc;

-- BlitzCache
select
    ID,
    ServerName,
    CheckDate,
    Version,
    QueryType,
    Warnings,
    DatabaseName,
    SerialDesiredMemory,
    SerialRequiredMemory,
    AverageCPU,
    TotalCPU,
    PercentCPUByType,
    CPUWeight,
    AverageDuration,
    TotalDuration,
    DurationWeight,
    PercentDurationByType,
    AverageReads,
    TotalReads,
    ReadWeight,
    PercentReadsByType,
    AverageWrites,
    TotalWrites,
    WriteWeight,
    PercentWritesByType,
    ExecutionCount,
    ExecutionWeight,
    PercentExecutionsByType,
    ExecutionsPerMinute,
    PlanCreationTime,
    PlanCreationTimeHours,
    LastExecutionTime,
    LastCompletionTime,
    PlanHandle,
    [Remove Plan Handle From Cache],
    SqlHandle,
    [Remove SQL Handle From Cache],
    [SQL Handle More Info],
    QueryHash,
    [Query Hash More Info],
    QueryPlanHash,
    StatementStartOffset,
    StatementEndOffset,
    PlanGenerationNum,
    MinReturnedRows,
    MaxReturnedRows,
    AverageReturnedRows,
    TotalReturnedRows,
    QueryText,
    NumberOfPlans,
    NumberOfDistinctPlans,
    MinGrantKB,
    MaxGrantKB,
    MinUsedGrantKB,
    MaxUsedGrantKB,
    PercentMemoryGrantUsed,
    AvgMaxMemoryGrant,
    MinSpills,
    MaxSpills,
    TotalSpills,
    AvgSpills,
    QueryPlanCost,
    JoinKey
from
    dbo.BlitzCache
where
    CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by
    TotalDuration desc;

-- BlitzFirst_Perfmon
select *
from dbo.BlitzFirst_PerfmonStats_Deltas
where CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by cntr_delta desc;

-- BlitzFirst_FileStats
select *
from dbo.BlitzFirst_FileStats_Deltas
where CheckDate = '2021-04-20 06:45:03.2938795 -04:00'
order by num_of_reads desc;
