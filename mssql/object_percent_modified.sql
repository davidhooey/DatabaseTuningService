use ApplicationData;

select
	object_schema_name(i.id) object_schema,
    tbls.name as table_name,
    i.name as index_name,
	i.indid,
    i.rowmodctr as modified_rows,
	i.rowcnt,
    convert(decimal(18,8), i.rowmodctr / i.rowcnt) as modified_percent,
    stats_date(i.id, i.indid) as last_stats_update
from
    sysindexes i
    inner join
    sysobjects tbls
        on i.id = tbls.id
    inner join
    sysusers schemas
        on tbls.uid = schemas.uid
    inner join
    information_schema.tables tl
        on tbls.name = tl.table_name
        and schemas.name = tl.table_schema
        and tl.table_type = 'BASE TABLE'
where
    0 < i.indid
and
    tl.table_schema <> 'sys'
and
    i.rowmodctr <> 0
and
	i.rowcnt <> 0
order by
     modified_percent desc;
