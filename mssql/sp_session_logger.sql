if object_id('dbo.sp_session_logger') is null
    exec ('create procedure dbo.sp_session_logger as return 0;');
go

alter procedure dbo.sp_session_logger
    @output_database_name nvarchar(256) = null,
    @output_schema_name nvarchar(256) = null,
    @output_table_name nvarchar(256) = null
with
    recompile
as
    set nocount on;
    set transaction isolation level read uncommitted;

    begin
    declare
        @valid_output_location bit,
        @create_table_string nvarchar(4000),
        @insert_string nvarchar(4000);

    select
        @valid_output_location = 0,
        @output_database_name = quotename(@output_database_name),
        @output_schema_name = quotename(@output_schema_name),
        @output_table_name = quotename(@output_table_name);

    begin
        if @output_database_name is not null
            and @output_schema_name is not null
            and @output_table_name is not null
            and exists (select *
            from sys.databases
            where quotename(name) = @output_database_name)
        begin
            set @valid_output_location = 1;
        end;
        else if @output_database_name is not null
            and @output_schema_name is not null
            and @output_table_name is not null
            and not exists (select *
            from sys.databases
            where quotename(name) = @output_database_name)
        begin
            raiserror('The specified output database was not found on this server', 16, 0);
        end;
        else
        begin
            set @valid_output_location = 0;
        end;
    end;

    if @valid_output_location = 1
    begin
        set @create_table_string = 'use '
            + @output_database_name
            + '; if exists(select * from '
            + @output_database_name
            + '.information_schema.schemata where quotename(schema_name) = '''
            + @output_schema_name
            + ''') and not exists (select * from '
            + @output_database_name
            + '.information_schema.tables where quotename(table_schema) = '''
            + @output_schema_name + ''' and quotename(table_name) = '''
            + @output_table_name + ''') create table '
            + @output_schema_name + '.'
            + @output_table_name
            + ' (
                id int identity(1,1) not null,
                log_datetime datetime not null,
                db_name nvarchar(128),
                session_id smallint not null,
                login_time datetime not null,
                host_name nvarchar(128),
                program_name nvarchar(128),
                host_process_id int,
                client_version int,
                client_interface_name nvarchar(32),
                security_id varbinary(85) not null,
                login_name nvarchar(128) not null,
                nt_domain nvarchar(128),
                nt_user_name nvarchar(128),
                status nvarchar(30) not null,
                context_info varbinary(128),
                cpu_time int not null,
                memory_usage int not null,
                total_scheduled_time int not null,
                total_elapsed_time int not null,
                endpoint_id int not null,
                last_request_start_time datetime not null,
                last_request_end_time datetime,
                reads bigint not null,
                writes bigint not null,
                logical_reads bigint not null,
                is_user_process bit not null,
                text_size int not null,
                language nvarchar(128),
                date_format nvarchar(3),
                date_first smallint not null,
                quoted_identifier bit not null,
                arithabort bit not null,
                ansi_null_dflt_on bit not null,
                ansi_defaults bit not null,
                ansi_warnings bit not null,
                ansi_padding bit not null,
                ansi_nulls bit not null,
                concat_null_yields_null bit not null,
                transaction_isolation_level smallint not null,
                lock_timeout int not null,
                deadlock_priority int not null,
                row_count bigint not null,
                prev_error int not null,
                original_security_id varbinary(85) not null,
                original_login_name nvarchar(128) not null,
                last_successful_logon datetime,
                last_unsuccessful_logon datetime,
                unsuccessful_logons bigint,
                group_id int not null,
                database_id smallint not null,
                authenticating_database_id int,
                open_transaction_count int
                constraint [pk_' + cast(newid() as char(36)) + '] primary key clustered (id asc)); ';

        set @insert_string = 'insert into '
            + @output_schema_name + '.' + @output_table_name
            + '(log_datetime, db_name, session_id, login_time, host_name, program_name, host_process_id, '
            + 'client_version, client_interface_name, security_id, login_name, nt_domain, nt_user_name, '
            + 'status, context_info, cpu_time, memory_usage, total_scheduled_time, total_elapsed_time, '
            + 'endpoint_id, last_request_start_time, last_request_end_time, reads, writes, logical_reads, '
            + 'is_user_process, text_size, language, date_format, date_first, quoted_identifier, arithabort, '
            + 'ansi_null_dflt_on, ansi_defaults, ansi_warnings, ansi_padding, ansi_nulls, concat_null_yields_null, '
            + 'transaction_isolation_level, lock_timeout, deadlock_priority, row_count, prev_error, '
            + 'original_security_id, original_login_name, last_successful_logon, last_unsuccessful_logon, '
            + 'unsuccessful_logons, group_id, database_id, authenticating_database_id, open_transaction_count) '
            + 'select '
            + 'sysdatetimeoffset(), db_name(database_id), session_id, login_time, host_name, program_name, host_process_id, '
            + 'client_version, client_interface_name, security_id, login_name, nt_domain, nt_user_name, '
            + 'status, context_info, cpu_time, memory_usage, total_scheduled_time, total_elapsed_time, '
            + 'endpoint_id, last_request_start_time, last_request_end_time, reads, writes, logical_reads, '
            + 'is_user_process, text_size, language, date_format, date_first, quoted_identifier, arithabort, '
            + 'ansi_null_dflt_on, ansi_defaults, ansi_warnings, ansi_padding, ansi_nulls, concat_null_yields_null, '
            + 'transaction_isolation_level, lock_timeout, deadlock_priority, row_count, prev_error, '
            + 'original_security_id, original_login_name, last_successful_logon, last_unsuccessful_logon, '
            + 'unsuccessful_logons, group_id, database_id, authenticating_database_id, open_transaction_count '
            + 'from sys.dm_exec_sessions order by session_id;';
        if @valid_output_location = 1
        begin
            set @create_table_string = replace(@create_table_string, ''''+@output_schema_name+'''', ''''+@output_schema_name+'''');
            set @create_table_string = replace(@create_table_string, ''''+@output_table_name+'''', ''''+@output_table_name+'''');
            exec(@create_table_string);

            set @insert_string = replace(@insert_string, ''''+@output_schema_name+'''', ''''+@output_schema_name+'''');
            set @insert_string = replace(@insert_string, ''''+@output_table_name+'''', ''''+@output_table_name+'''');
            exec(@insert_string);
        end;
    end;
end;

    set nocount off;
go

/*
execute dbo.sp_session_logger
    @output_database_name = 'loggers',
    @output_schema_name = 'dbo',
    @output_table_name = 'session_log';
*/
