missing_indexes.sql
===================

The query optimizer generates an execution plan for a query and determines the
optimal access path. If the ideal index does not exist, the optimizer chooses
the best one available or performs a table scan. However, the optimizer stores
the details of the missing index.

Suggested indexes should be fully tested to ensure they do not cause
performance regressions.

The SQL Server Query Optimization Team at Microsoft uses the following formula
for calculating the overall benefit of a suggested index, based on columns in
the sys.dm_db_missing_index_group_stats.

(user_seeks + user_scans) * avg_total_user_cost * (avg_user_impact * 0.01)


### Column Details

Colum               | Description
------------------- | ---------------------------------------------------------
index_advantage     | Calculated benefit of suggested index.
last_user_seek      | Last time a seek operation might have used the index.
table               | Table name.
equality_columns    | Columns that would have been useful based on equality predicate.
inequality_columns  | Columns that would have been useful based on inequality predicate.
included_columns    | Columns that, if included, would have been useful to cover the query.
unique_compiles     | Number of plans that have been compiled/recompiled that might have used the index.
user_seeks          | Number of seek operations in user queries that might have used the index.
user_scans          | Number of scan operations in user queries that might have used the index.
avg_total_user_cost | Average cost saving for the queries that could have been helped bu the index.
avg_user_impact     | Estimated percentatge by which the average query cost would drop, for queries that could use this index.


### Required Permission

VIEW SERVER STATE
