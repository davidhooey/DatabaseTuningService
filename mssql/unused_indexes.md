unused_indexes.sql
==================

Provides list of indexes that have never been written to or read from
since last service restart. The longer the server has been up, the more
valuable this information is. This also indicates the underlying table
has not been used.


### Column Details

Column           | Description
---------------- | ------------------------------------------------------------
table_name       | Table name.
index_name       | Index name.
index_type       | Type of index, CLUSTERED or NONCLUSTERED.
