file_stats.sql
==============

Lists the file IO statisticis for the selected database.

Refer to [Are I/O latencies killing your performance?](https://www.sqlskills.com/blogs/paul/are-io-latencies-killing-your-performance/).
Typical I/O latency could fall into the following ranges:

Range          | Latency
---------------|----------------
Excellent      | < 1ms
Very good      | < 5ms
Good           | 5ms – 10ms
Poor           | 10ms – 20ms
Bad            | 20ms – 100ms
Shockingly bad | 100ms – 500ms
WOW!           | > 500ms


### Column Details

Column                    | Description
--------------------------|---------------------------------------------------------------------------------
database_id               | Database ID.
database_name             | Database name.
file_id                   | File ID.
name                      | Logical name of the file in the database.
type_desc                 | File type, ROWS for datafiles, LOG for transaction log.
physical_name             | OS file name.
size                      | Current size of the file in 8KB pages.
sample_ms                 | Number of milliseconds since the computer was started.
num_of_reads              | Number of reads issued on the file.
num_of_bytes_read         | Total number of bytes read on this file.
io_stall_read_ms          | Total time, in milliseconds, that users waited for reads issued on this file.
io_stall_average_read_ms  | Average time, in milliseconds, that users waited for reads issued on this file.
io_stall_queued_read_ms   | Total IO latency by IO resource governance for reads. SQL Server 2014 and later.
num_of_writes             | Number of writes made on this file.
num_of_bytes_written      | Total number of bytes written to the file.
io_stall_write_ms         | Total time, in milliseoncds, that users waited for writes to be completed on this file.
io_stall_average_write_ms | Average time, in milliseconds, that users waited for writes issued on this file.
io_stall_queued_write_ms  | Total IO latency by IO resource governance for writes. SQL Server 2014 and later.
io_stall                  | Total time, in millisoncds, that users waited for I/O to be completed on the file.
size_on_disk_bytes        | Number of bytes used on this disk for this file.
file_handle               | Windows file handle for this file.

### Required Permission

VIEW SERVER STATE
