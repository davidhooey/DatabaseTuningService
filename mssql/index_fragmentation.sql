use ApplicationDatabase;

select
    db_name(db_id()) as database_name,
    object_schema_name(ddips.object_id) + '.' + object_name(ddips.object_id, db_id()) as object_name,
    i.name as index_name,
    ddips.index_type_desc,
    ddips.partition_number,
    ddips.alloc_unit_type_desc,
    ddips.index_depth,
    ddips.index_level,
    round(ddips.avg_fragmentation_in_percent, 0) as avg_fragmentation_in_percent,
    ddips.avg_page_space_used_in_percent,
    ddips.avg_fragment_size_in_pages,
    ddips.fragment_count,
    ddips.page_count,
    ddips.record_count,
    ddips.min_record_size_in_bytes,
    ddips.max_record_size_in_bytes,
    ddips.avg_record_size_in_bytes,
    case
    when round(ddips.avg_fragmentation_in_percent,0) >= 40 then
        'alter index ' +
        i.name +
        ' on ' +
        object_schema_name(ddips.object_id) +
        '.' +
        object_name(ddips.object_id, db_id()) +
        ' rebuild;'
    else
        'alter index ' +
        i.name +
        ' on ' +
        object_schema_name(ddips.object_id) +
        '.' +
        object_name(ddips.object_id, db_id()) +
        ' reorganize;'
    end as recommendation
from
    sys.dm_db_index_physical_stats(db_id(), null, null, null, 'DETAILED') ddips
    inner join
    sys.indexes i
        on ddips.object_id = i.object_id
        and ddips.index_id = i.index_id
where
    ddips.index_type_desc <> 'HEAP'
and
    ddips.avg_fragmentation_in_percent > 10
and
    ddips.page_count > 1000
order by
    ddips.avg_fragmentation_in_percent desc,
    object_name(ddips.object_id, db_id()),
    i.name;
