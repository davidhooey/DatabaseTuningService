unread_indexes.sql
==================

List of indexes which have never been choosen for use by the query optimizer
since the service was started. The longer the server has been up, the more
valuable this information is. These indexes are being maintained but are not
being used.


### Column Details

Column           | Description
---------------- | ------------------------------------------------------------
database_name    | Database name.
table_name       | Table name.
index_name       | Index name.
index_type       | Type of index, CLUSTERED or NONCLUSTERED.
user_reads       | Sum of user_seeks, user_scans, and user_lookups for a total number of reads.
user_writes      | Number of updates by user queries.


### Required Permission

VIEW SERVER STATE
