# Statspack

## Statspack Setup

1. If necessary setup the Statspack user and schema. The Statspack user is called `PERFSTAT`. The DBA may wish to change the `default_tablespace` and password for the `PERFSTAT` user.

    ```sql
    connect / as sysdba
    define default_tablespace='USERS'
    define temporay_tablespace='TEMP'
    define perfstat_password='oracle'
    @?/rdbms/admin/spcreate.sql
    ```

## Using Statspack

1. Using the `perfstat` user, create an initial snapshot and determine its `snap_id`. Remember the `snap_id`.

    ```sql
    connect perfstat/oracle
    execute statspack.snap
    select max(snap_id) snap_id from stats$snapshot;
    ```

2. Reproduce the performance issue.

3. Using `perfstat` user, create a second snapshot and determine its `snap_id`. Remember the `snap_id`.

    ```sql
    connect perfstat/oracle
    execute statspack.snap
    select max(snap_id) snap_id from stats$snapshot;
    ```

4. Using the `perfstat` user take the initial `snap_id` and the second `snap_id` to generate a Stackpack report. The report will be created in the directory in which SQL*Plus was run.

    ```sql
    connect perfstat/oracle
    @?/rdbms/admin/spreport.sql
    Enter value for begin_snap: [ENTER INITIAL SNAP_ID FROM STEP 1]
    Enter value for end_snap: [ENTER END SNAP_ID FROM STEP 3]
    Enter value for report_name: [ENTER A REPORT NAME OR TAKE THE DEFAULT]
    ```
