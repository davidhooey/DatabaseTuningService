select
    utm.table_name,
    uts.num_rows,
    utm.inserts,
    utm.updates,
    utm.deletes,
    (utm.inserts + utm.updates + utm.deletes) as total_changes,
    round((utm.inserts + utm.updates + utm.deletes) / greatest(uts.num_rows, 1) * 100, 2) as change_rate_percent,
    timestamp as last_modified
from
    user_tab_modifications utm
    left join
    user_tab_statistics uts
        on utm.table_name = uts.table_name
where
    uts.num_rows > 1000
order by
    change_rate_percent desc;
