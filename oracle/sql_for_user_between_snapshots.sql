select
    *
from
    dba_hist_sqlstat
where
    snap_id in(19065, 19066)
and
    parsing_schema_name = 'CS_23_3'
order by
    elapsed_time_delta desc;
