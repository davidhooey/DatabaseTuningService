SQLHC
=====

1. Download via Oracle Support DocID 1366133.1.

2. Login to Oracle as `SYS`, as user with the `DBA` role, or a user with access
   to the data dictionary views.

3. Determine a SQL_ID in need of tuning. It must be memory resident within the
   Shared Pool.

4. Determine the Oracle Pack license being used (Tuning (`T`), Diagnostics
   (`D`) or None (`N`)).

5. Execute the `sqlhc.sql` script passing in the Oracle Pack license `T`, `D`,
   or `N` and the SQL_ID. A set of HTML reports will be generated.
```
SQL> @sqlhc.sql T g4pkmrqrqxg3b
```
