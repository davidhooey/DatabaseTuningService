select distinct
    ash.event,
    ash.sql_id,
    users.username,
    ash.program,
    dbms_lob.substr(sql_text.sql_text, 4000, 1) sql_text
from
    dba_hist_active_sess_history ash
    left outer join
    dba_users users
        on ash.user_id = users.user_id
    left outer join
    dba_hist_sqltext sql_text
        on ash.sql_id = sql_text.sql_id
where
    ash.snap_id between BEGIN_SNAP_ID and END_SNAP_ID
and
    ash.event = 'wait event'
