create or replace trigger [SCHEMA_NAME]_startsqltracing after logon on [SCHEMA_NAME].schema
begin
    execute immediate 'alter session set timed_statistics=true';
    execute immediate 'alter session set max_dump_file_size=unlimited';
    execute immediate 'alter session set events ''10046 trace name context forever, level 12''';
end;
/


create or replace trigger [SCHEMA_NAME]_endsqltracing before logoff on [SCHEMA_NAME].schema
begin
    execute immediate 'alter session set events ''10046 trace name context off''';
end;
/

drop trigger [SCHEMA_NAME]_startsqltracing;
drop trigger [SCHEMA_NAME]_endsqltracing;
