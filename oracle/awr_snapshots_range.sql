select
    dbid,
    instance_number,
    min(begin_interval_time) min_time,
    min(snap_id) min_snap_id,
    max(begin_interval_time) max_time,
    max(snap_id) max_snap_id,
    count(*) snapshot_count
from
    dba_hist_snapshot
group by
    dbid,
    instance_number
order by
    dbid,
    instance_number;
