select
    d.tablespace_name tn,
    d.block_size bs,
    d.extent_management lm,
    d.segment_space_management assm,
    d.status st,
    to_char(f.bytes/1024,'999,999,999')||'K' bts,
    to_char((f.bytes-s.free_bytes)/1024,'999,999,999')||'K' used,
    to_char(round((f.bytes-s.free_bytes)/f.bytes*100),'990.9')||'%' pct,
    case trunc(33*(f.bytes-s.free_bytes)/f.bytes)
    when 0 then
        rpad('|',33,'-')||'|'
    else
        rpad(rpad('|',(33*(f.bytes-s.free_bytes)/f.bytes),'X'),33,'-')||'|'
    end gr
from
    sys.dba_tablespaces d,
    (
        select
            tablespace_name,
            sum(bytes) bytes
        from
            dba_data_files
        group by
            tablespace_name
    ) f,
    (
        select
            tablespace_name,
            sum(bytes) free_bytes
        from
            sys.dba_free_space
        group by
            tablespace_name
    ) s
where
    d.tablespace_name = f.tablespace_name
and
    d.tablespace_name = s.tablespace_name
order by
    d.tablespace_name;
