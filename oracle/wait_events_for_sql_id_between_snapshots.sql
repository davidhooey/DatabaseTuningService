select
    sql_id,
    event,
    time_waited,
    case when time_waited = 0 then
        0
    else
        round(time_waited*100 / sum(time_waited) Over(partition by sql_id), 2)
    end percentage
from
    (
        select
            sql_id,
            event,
            sum(time_waited) time_waited
        from
            dba_hist_active_sess_history
        where
            sql_id in('SQL_ID1', 'SQL_ID2')
        and
            snap_id between BEGIN_SNAP_ID and END_SNAP_ID
        group by
            sql_id,
            event
    )
order by
    sql_id,
    time_waited desc;
