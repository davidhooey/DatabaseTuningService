Oracle Database Tuning Service - Checklist
==========================================

Performance Data Collection
---------------------------

### Performance Statistics

* Connect logs as long as possible to trap typical workload
* AWR export. If export is not possible, generated daily and hourly reports
  for the same period as connect logs.
* System performance data, vmstat
* User performance complaints


### System

* CPU
* Disk
* Network
* RAM


### Oracle Instance

* Initialization parameters.
* SGA memory allocations
    - Shared Pool
    - Buffer Cache
    - PGA
    - Large Pool
* Alert log
* Trace logs
* Listener log


### Oracle Storage

* Control files
* Redo logs
* Tablespace
    - Locally managed
    - Automatic segment space management (ASSM)
    - Block size
* Datafiles


### Schema

* Object statistics
* Histograms
* Segments
    - Table fragmentation
    - Index fragmentation
    - Logging


Performance Analysis
--------------------

* Connect logs
    - CSQLAnalyzer to determine top queries.
    - Oracle Errors
* AWR Report Generation
    - If an export of the AWR was received, import and use OPerf to generate
      hourly and daily AWR/ADDM reports.
* AWR Report Analysis
    - Top Wait Events
    - Top SQL
    - Buffer Pool Advisor
    - PGA Advisor
    - ADDM Recommendations


Administration
--------------

* Database backup and recovery meeting SLA.
* System backup.
* High availability recommended.
* Recommend dbverify.


Application Data
----------------
* Database verification errors
* Queue table sizes.
