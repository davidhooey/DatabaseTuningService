-------------------------------------------------------------------------------
--     NAME
--         statistics_advisor.sql
--
--     DESCRIPTION
--         SQL*Plus command file to run the Statistics Advisor for all segments
--         in a schema.
--
--     NOTES
--         Execute the script as the schema owner. The results will be written
--         to statistics_advisor.out.
--
--         sqlplus SchemaOwner @statistics_advisor.sql
--
--     REQUIREMENTS
--         Oracle 12.2 is required.
--         The schema owner requires the ADVISOR role. This role can be revoked
--         after running the script.
--
--         grant advisor to [SchemaOwner];
--         revoke advisor from [SchemaOwner];
--
--     HISTORY
--         David Hooey - 2019-12-17
-------------------------------------------------------------------------------

set echo off
set feedback off
set verify off
set linesize 3000
set long 500000
set longchunksize 100000
set pagesize 0
set serveroutput on size unlimited

spool statistics_advisor.out

declare
    v_tname                 varchar2(128);
    filter                  dbms_stats.StatsAdvFilter := null;
    filterTab               dbms_stats.StatsAdvFilterTab := null;
    v_ename                 varchar2(128) := null;
    v_report                clob := null;
    v_filterReport          clob := null;
    v_counter               number := 0;
    obj                     dbms_stats.ObjectElem;
    objTab                  dbms_stats.ObjectTab;
    v_objCnt                number := 0;
begin
    -- Create a task
    v_tname := dbms_stats.create_advisor_task(v_tname);

    -- Initialize filter table
    filterTab := DBMS_STATS.StatsAdvFilterTab();

    -- Filter for current schema
    filter.include := true;

    objTab := dbms_stats.ObjectTab();
    v_objCnt := 0;

    select user into obj.ownname from dual;
    obj.objname := null;

    -- add to the object table
    v_objCnt := v_objCnt + 1;
    objTab.extend;
    objTab(v_objCnt) := obj;

    filter.objlist := objTab;

    -- Add the object filter to the filter table
    v_counter := v_counter + 1;
    filterTab.extend;
    filterTab(v_counter) := filter;

    -- Add filterTab to advisor task.
    v_filterReport := dbms_stats.configure_advisor_filter
    (
        task_name          => v_tname,
        stats_adv_opr_type => null,
        configuration_type => 'SET',
        filter             => filterTab
    );

    -- execute the task
    v_ename := dbms_stats.execute_advisor_task(v_tname);

    -- view the task report
    v_report := dbms_stats.report_advisor_task(v_tname, v_ename, 'TEXT', 'ALL', 'ALL');
    dbms_output.put_line(v_report);
end;
/

spool off;

quit;
