select
    ssc.*,
    sa.version_count,
    sa.sql_text
from
    v$sqlarea sa
    inner join
    v$sql_shared_cursor ssc
        on sa.address = ssc.address
where
    sa.sql_id = 'SQL_ID'
order by
    ssc.child_number;
