select * from (
select
    s.dbid dbid,
    s.instance_number instance_number,
    s.snap_id snap_id,
    s.begin_interval_time,
    s.end_interval_time,
    swap_out.stat_name stat_name,
    swap_out.value vm_bytes,
    swap_out.value - lag(swap_out.value) over(order by s.dbid, s.instance_number, s.snap_id) as vm_bytes_delta,
    round((swap_out.value - lag(swap_out.value) over(order by s.dbid, s.instance_number, s.snap_id))/1024/1024, 1) as vm_mb_delta
from
    dba_hist_osstat swap_out
    inner join
    dba_hist_snapshot s
        on swap_out.dbid = s.dbid
        and swap_out.snap_id = s.snap_id
where
    swap_out.stat_name = 'VM_OUT_BYTES'
union
select
    s.dbid dbid,
    s.instance_number instance_number,
    s.snap_id snap_id,
    s.begin_interval_time,
    s.end_interval_time,
    swap_in.stat_name stat_name,
    swap_in.value vm_bytes,
    swap_in.value - lag(swap_in.value) over(order by s.dbid, s.instance_number, s.snap_id) as vm_bytes_delta,
    round((swap_in.value - lag(swap_in.value) over(order by s.dbid, s.instance_number, s.snap_id))/1024/1024, 1) as vm_mb_delta
from
    dba_hist_osstat swap_in
    inner join
    dba_hist_snapshot s
        on swap_in.dbid = s.dbid
        and swap_in.snap_id = s.snap_id
where
    swap_in.stat_name = 'VM_IN_BYTES'
)
order by
    dbid,
    instance_number,
    snap_id,
    stat_name;
