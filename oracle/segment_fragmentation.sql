-------------------------------------------------------------------------------
--     NAME
-- 	       segment_framentation.sql
--
--     DESCRIPTION
--         SQL*Plus command file which collects and reports on segment space
--         information. The Segment Advisor is also run.
--
--     NOTES
--         Execute the script as the schema owner. The results will be written
--         to segment_fragmentation.out.
--
--         sqlplus SchemaOwner @segment_fragmentation.sql
--
--     REQUIREMENTS
--         The schema owner requires the ADVISOR and SELECT ANY DICTIONARY
--         roles. These roles can be revoked after running the script.
--
--         grant advisor, select any dictionary to [SchemaOwner];
--         revoke advisor, select any dictionary from [SchemaOwner];
--
--     HISTORY
--         David Hooey - 2013-03-07
-------------------------------------------------------------------------------

set echo off
set feedback off
set verify off
set linesize 80
set serveroutput on size unlimited

--
-- Create Temporary Table
--

begin
    execute immediate 'drop table segment_fragmentation';
exception when others then
    if sqlcode != -942 then
        raise;
    end if;
end;
/

create table segment_fragmentation
(
    owner                     varchar2(30),
    segment_name              varchar2(30),
    segment_type              varchar2(30),
    tablespace_name           varchar2(30),
    extent_management         varchar2(10),
    segment_space_management  varchar2(6),
    unformatted_blocks        number,
    unformatted_bytes         number,
    fs1_blocks                number,
    fs1_bytes                 number,
    fs2_blocks                number,
    fs2_bytes                 number,
    fs3_blocks                number,
    fs3_bytes                 number,
    fs4_blocks                number,
    fs4_bytes                 number,
    full_blocks               number,
    full_bytes                number,
    total_blocks              number,
    total_bytes               number,
    unused_blocks             number,
    unused_bytes              number,
    last_used_extent_file_id  number,
    last_used_extent_block_id number,
    last_used_block           number,
    timestamp                 date
);


--
-- Data Gathering
--

declare
    v_unformatted_blocks        number;
    v_unformatted_bytes         number;
    v_fs1_blocks                number;
    v_fs1_bytes                 number;
    v_fs2_blocks                number;
    v_fs2_bytes                 number;
    v_fs3_blocks                number;
    v_fs3_bytes                 number;
    v_fs4_blocks                number;
    v_fs4_bytes                 number;
    v_full_blocks               number;
    v_full_bytes                number;
    v_total_blocks              number;
    v_total_bytes               number;
    v_unused_blocks             number;
    v_unused_bytes              number;
    v_last_used_extent_file_id  number;
    v_last_used_extent_block_id number;
    v_last_used_block           number;
begin
    for s in (select seg.segment_name, seg.segment_type, seg.tablespace_name,
                     ts.extent_management, ts.segment_space_management
              from user_tablespaces ts, user_segments seg
              where ts.tablespace_name = seg.tablespace_name
              and seg.segment_type in ('TABLE', 'INDEX','CLUSTER','LOB')
              and seg.segment_name not in (select object_name from user_recyclebin)
              order by seg.segment_name)
    loop
		if s.segment_space_management = 'AUTO' then
        	dbms_space.space_usage(user, s.segment_name, s.segment_type,
                                   v_unformatted_blocks, v_unformatted_bytes,
                                   v_fs1_blocks, v_fs1_bytes, v_fs2_blocks,
                                   v_fs2_bytes, v_fs3_blocks, v_fs3_bytes,
                                   v_fs4_blocks, v_fs4_bytes, v_full_blocks,
                                   v_full_bytes, NULL);
        end if;

        dbms_space.unused_space(user, s.segment_name, s.segment_type,
                                v_total_blocks, v_total_bytes,
                                v_unused_blocks, v_unused_bytes,
                                v_last_used_extent_file_id,
                                v_last_used_extent_block_id,
                                v_last_used_block, NULL);

        insert into segment_fragmentation
        (
            owner, segment_name, segment_type, tablespace_name,
            extent_management, segment_space_management, unformatted_blocks,
            unformatted_bytes, fs1_blocks, fs1_bytes, fs2_blocks, fs2_bytes,
            fs3_blocks, fs3_bytes, fs4_blocks, fs4_bytes, full_blocks,
            full_bytes, total_blocks, total_bytes, unused_blocks,
            unused_bytes, last_used_extent_file_id, last_used_extent_block_id,
            last_used_block, timestamp
        )
        values
        (
            user, s.segment_name, s.segment_type, s.tablespace_name,
            s.extent_management, s.segment_space_management,
            v_unformatted_blocks, v_unformatted_bytes, v_fs1_blocks,
            v_fs1_bytes, v_fs2_blocks, v_fs2_bytes, v_fs3_blocks, v_fs3_bytes,
            v_fs4_blocks, v_fs4_bytes, v_full_blocks, v_full_bytes,
            v_total_blocks, v_total_bytes, v_unused_blocks, v_unused_bytes,
            v_last_used_extent_file_id, v_last_used_extent_block_id,
            v_last_used_block, sysdate
        );
    end loop;

    commit;
end;
/


--
-- Reporting
--

spool segment_fragmentation.out

set linesize 30
select to_char(sysdate, 'Month DD, YYYY HH24:MI:SS') "Date" from dual;

set linesize 145 pagesize 10000
col tablespace_name for a30
col extent_management for a17
col segment_space_management for a24

TTITLE underline "Tablespaces"

select
    tablespace_name,
    extent_management,
    segment_space_management
from
    user_tablespaces
order by
    tablespace_name;


set linesize 155 pagesize 10000
col owner for a20
col fs1_blocks for 9999999 heading "blocks with|0-25% free space"
col fs2_blocks for 9999999 heading "blocks with|25-50% free space"
col fs3_blocks for 9999999 heading "blocks with|50-75% free space"
col fs4_blocks for 9999999 heading "blocks with|75-100% free space"
col full_blocks for 9999999 heading "full|blocks"
col total_blocks for 9999999 heading "total|blocks"
col percent_free for 990.9 heading "percentage|free"

TTITLE underline "ASSM Table Free Space"

select
    owner,
    segment_name,
    fs1_blocks,
    fs2_blocks,
    fs3_blocks,
    fs4_blocks,
    full_blocks,
    total_blocks,
    round((((fs1_blocks*0.125) + (fs2_blocks*0.375) + (fs3_blocks*0.625) + (fs4_blocks*0.875))/total_blocks)*100,1) percent_free
from
    segment_fragmentation
where
    segment_type = 'TABLE'
and
    extent_management = 'LOCAL'
and
    segment_space_management = 'AUTO'
order by
    percent_free desc;


TTITLE underline "ASSM Index Free Space"

select
    owner,
    segment_name,
    fs1_blocks,
    fs2_blocks,
    fs3_blocks,
    fs4_blocks,
    full_blocks,
    total_blocks,
    round(((fs1_blocks*0.125) + (fs2_blocks*0.375) + (fs3_blocks*0.625) + (fs4_blocks*0.875))/total_blocks,1) percent_free
from
    segment_fragmentation
where
    segment_type = 'INDEX'
and
    extent_management = 'LOCAL'
and
    segment_space_management = 'AUTO'
order by
    percent_free desc;


TTITLE underline "Top Ten Largest Empty Segments"

set linesize 160 pagesize 10000
col owner for a20
col extent_management for a10 heading "Extent|Management"
col segment_space_management for a10 heading "Segment|Space|Management"
col total_MB for 999999 heading "Total Free|Space (MB)"
clear breaks
clear computes
break on report dup
compute sum of total_MB on report

select
    owner,
    segment_name,
    segment_type,
    tablespace_name,
    extent_management,
    segment_space_management,
    total_bytes/1048576 total_MB
from
    segment_fragmentation
where
    full_bytes = 0
and
    trunc(total_bytes/1048576) > 0
order by
    total_bytes desc;

TTITLE off


--
-- Segment Advisor
--

declare
    v_task_name   varchar2(100);
    v_task_desc   varchar2(500);
    v_objid       number;
begin
    begin
        v_task_name := 'OT_SEGMENT_ADVISOR_RUN';
        v_task_desc := 'OT MANUAL SEGMENT ADVISOR RUN';

        -- Create Segment Advisor task.
        dbms_advisor.create_task
        (
            advisor_name => 'Segment Advisor',
            task_name    => v_task_name,
            task_desc    => v_task_desc
        );

        -- Add all segments to the task.
        for s in (select segment_name, segment_type
                  from user_segments
                  where segment_type in ('TABLE', 'INDEX', 'LOB')
                  and segment_name not in (select object_name from user_recyclebin))
        loop
            dbms_advisor.create_object
            (
                task_name   => v_task_name,
                object_type => s.segment_type,
                attr1       => user,
                attr2       => s.segment_name,
                attr3       => null,
                attr4       => null,
                attr5       => null,
                object_id   => v_objid
            );
        end loop;

        -- Set task parameter to recommend all.
        dbms_advisor.set_task_parameter
        (
            task_name => v_task_name,
            parameter => 'RECOMMEND_ALL',
            value     => 'TRUE'
        );

        -- Run Segment Advisor.
        dbms_advisor.execute_task(v_task_name);

    exception when others then
        dbms_output.put_line('Exception: ' || SQLERRM);
    end;

    -- Output findings.
    dbms_output.put_line(chr(10));
    dbms_output.put_line('Segment Advisor Recommendations');
    dbms_output.put_line('--------------------------------------------------------------------------------');

    for r in (select segment_owner, segment_name, segment_type, partition_name,
                     tablespace_name, allocated_space, used_space,
                     reclaimable_space, chain_rowexcess, recommendations, c1, c2, c3
              from table(dbms_space.asa_recommendations('TRUE', 'TRUE', 'FALSE'))
              where segment_owner = user
              order by reclaimable_space desc)
    loop
        dbms_output.put_line('');
        dbms_output.put_line('Owner              : ' || r.segment_owner);
        dbms_output.put_line('Segment            : ' || r.segment_name);
        dbms_output.put_line('Segment Type       : ' || r.segment_type);
        dbms_output.put_line('Partition Name     : ' || r.partition_name);
        dbms_output.put_line('Tablespace         : ' || r.tablespace_name);
        dbms_output.put_line('Allocated Space    : ' || r.allocated_space);
        dbms_output.put_line('Used Space         : ' || r.used_space);
        dbms_output.put_line('Reclaimable Space  : ' || r.reclaimable_space);
        dbms_output.put_line('Chain Rowexcess    : ' || r.chain_rowexcess);
        dbms_output.put_line('Recommendations    : ' || r.recommendations);
        dbms_output.put_line('Run First          : ' || r.c3);
        dbms_output.put_line('Run Second         : ' || r.c2);
        dbms_output.put_line('Run Third          : ' || r.c1);
        dbms_output.put_line('--------------------------------------------------------------------------------');
    end loop;

    -- Remove Segment Advisor task.
    dbms_advisor.delete_task(v_task_name);
end;
/

spool off;


--
-- Drop Temporary Table
--

drop table segment_fragmentation;

quit;
