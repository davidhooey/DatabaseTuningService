-------------------------------------------------------------------------------
-- NAME
--     statistics_csv.sql
--
-- DESCRIPTION
--     A SQLcl command file to output the schema statistics for a schema in
--     CSV format.
--
-- NOTES
--     Execute the script as the schema owner. The results will be written
--     to the following files.
--
--     table_statistics.csv
--     table_column_statistics.csv
--     table_histograms.csv
--     index_statistics.csv
--
--     sql SchemaOwner @statistics_csv.sql
--
-- REQUIREMENTS
--     SQLcl (https://www.oracle.com/database/technologies/appdev/sqlcl.html)
--     Oracle 12c and later.
--
-- HISTORY
--     David Hooey - 2020-10-08
-------------------------------------------------------------------------------

set feedback off
set heading on
set pagesize 0
set termout off
set wrap on

set sqlformat csv

--
-- Table Statistics
--

spool table_statistics.csv

select
    table_name,
    partition_name,
    partition_position,
    subpartition_name,
    subpartition_position,
    object_type,
    num_rows,
    blocks,
    empty_blocks,
    avg_space,
    chain_cnt,
    avg_row_len,
    avg_space_freelist_blocks,
    num_freelist_blocks,
    avg_cached_blocks,
    avg_cache_hit_ratio,
    im_imcu_count,
    im_block_count,
    im_stat_update_time,
    scan_rate,
    sample_size,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    global_stats,
    user_stats,
    stattype_locked,
    stale_stats,
    scope
from
    user_tab_statistics
order by
    table_name,
    partition_position,
    subpartition_position;

spool off

--
-- Table Column Statistics
--

spool table_column_statistics.csv

select
    table_name,
    column_name,
    num_distinct,
    low_value,
    high_value,
    density,
    num_nulls,
    num_buckets,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    sample_size,
    global_stats,
    user_stats,
    notes,
    avg_col_len,
    histogram,
    scope
from
    user_tab_col_statistics
order by
    table_name,
    column_name;

spool off

--
-- Index Statistics
--

spool index_statistics.csv

select
    table_name,
    index_name,
    table_owner,
    partition_name,
    partition_position,
    subpartition_name,
    subpartition_position,
    object_type,
    blevel,
    leaf_blocks,
    distinct_keys,
    avg_leaf_blocks_per_key,
    avg_data_blocks_per_key,
    clustering_factor,
    num_rows,
    avg_cached_blocks,
    avg_cache_hit_ratio,
    sample_size,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    global_stats,
    user_stats,
    stattype_locked,
    stale_stats,
    scope
from
    user_ind_statistics
order by
    table_name,
    index_name;

spool off

--
-- Histogram Statistics
--

spool table_histograms.csv

select
    table_name,
    column_name,
    endpoint_number,
    endpoint_value,
    endpoint_actual_value,
    endpoint_actual_value_raw,
    endpoint_repeat_count,
    scope
from
    user_tab_histograms
order by
    table_name,
    column_name;

spool off

quit
