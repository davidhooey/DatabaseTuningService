-- SQL*Plus script to generate a html and text based AWR SQL report.
--
-- Arguments
--
-- dbid: Database ID
-- inst: Instance ID
-- bid: Begin snapshot ID
-- eid: End snapshot ID
-- sqlid: SQL ID
--
-- Syntax:
--
-- sqlplus username@database @awrsqrpt_sql.sql dbid inst bid eid sqlid
--
-- Example:
--
-- sqlplus system@orcl122 @awrsqrpt_sql.sql 3577477364 1 47974 47986 cutxr98c0pq4n
--
-- Find the snapshots.
--
-- select
--     snap_id,
--     dbid,
--     instance_number,
--     begin_interval_time,
--     end_interval_time
-- from
--     dba_hist_snapshot
-- order by
--     snap_id desc;
--

set feedback off
set heading off
set linesize 10000
set pagesize 0
set termout off
set trimspool on
set verify off

define dbid=&1
define inst=&2
define bid=&3
define eid=&4
define sqlid=&5

column awr_html_file new_value awr_html_file noprint
column awr_text_file new_value awr_text_file noprint

select 'awr_sql_report_' || &&dbid || '_' || &&inst || '_' || &&bid || '_' || &&eid || '_' || '&&sqlid' || '.html' awr_html_file from dual;
select 'awr_sql_report_' || &&dbid || '_' || &&inst || '_' || &&bid || '_' || &&eid || '_' || '&&sqlid' || '.txt' awr_text_file from dual;

set termout on
prompt Generating html AWR SQL report
set termout off

spool &awr_html_file

select
    output
from
    table(
        dbms_workload_repository.awr_sql_report_html(
            l_dbid       => &&dbid,
            l_inst_num   => &&inst,
            l_bid        => &&bid,
            l_eid        => &&eid,
            l_sqlid        => '&&sqlid'
        )
    );

spool off

set termout on
prompt Generating text AWR SQL report
set termout off

spool &awr_text_file

select
    output
from
    table(
        dbms_workload_repository.awr_sql_report_text(
            l_dbid       => &&dbid,
            l_inst_num   => &&inst,
            l_bid        => &&bid,
            l_eid        => &&eid,
            l_sqlid        => '&&sqlid'
        )
    );

spool off

quit
