-- sql_tune_sqlid.sql
--
--    NAME
--      sql_tune_sqlid.sql
--
--    DESCRIPTION
--	    SQL*Plus command file which analyzes a single SQL
--      statement using the DBMS_SQLTUNE package which in
--      turn uses the SQL Tuning Advisor in Oracle 10g.
--
--    NOTES
--      Execute the script as the SYS user or as a user
--      with the ADVISOR system privilege.
--

spool sql_tune_sqlid.out

set echo off;
set feedback off;
set term off;
set pagesize 0;
set linesize 150;
set long 2000000000;
set longchunk 1000;

variable task varchar2(64);

exec :task := dbms_sqltune.create_tuning_task(sql_id => 'SQL_ID1');
exec dbms_sqltune.execute_tuning_task(:task);
select dbms_sqltune.report_tuning_task(:task,'TEXT','ALL','ALL') from dual;
select dbms_sqltune.script_tuning_task(:task,'ALL') from dual;
exec dbms_sqltune.drop_tuning_task(:task);

spool off;

quit;
