select
    sql_id,
    event,
    time_waited "time_waited(s)",
    case when time_waited = 0 then
        0
    else
        round(time_waited*100 / sum(time_waited) Over(), 2)
    end "percentage"
from
    (
        select sql_id, event, sum(time_waited) time_waited
        from dba_hist_active_sess_history
        where snap_id in(BEGIN_SNAP_ID, END_SNAP_ID)
        and event is not null
        and user_id = (select user_id from dba_users where username = 'USERNAME')
        group by sql_id, event
    )
order by
    sql_id,
    time_waited desc;
