-------------------------------------------------------------------------------
--     NAME
--         index_framentation.sql
--
--     DESCRIPTION
--         SQL*Plus command file which reports index fragmentation.
--
--     NOTES
--         Execute the script as the schema owner. The results will be written
--         to index_fragmentation.out.
--
--         sqlplus SchemaOwner @index_fragmentation.sql
--
--     REQUIREMENTS
--         None.

--     HISTORY
--         David Hooey - 2013-03-07
-------------------------------------------------------------------------------

set echo off
set feedback off
set verify off
set serveroutput on size unlimited
set linesize 100

spool index_fragmentation.out

declare
    pragma autonomous_transaction;

    vpercent        number;
    vlf_rows_len    number;
begin
    dbms_output.put_line('Index Fragmentation');
    dbms_output.put_line('------------------------------------------------------------------------------------------');

    for i in (select index_name from user_indexes order by index_name)
    loop
        begin
            dbms_output.put_line('Index Name     : ' || i.index_name);

            execute immediate 'analyze index '|| i.index_name ||' validate structure';

            select lf_rows_len into vlf_rows_len
            from index_stats;

            if vlf_rows_len > 0 then

                select round((del_lf_rows_len/lf_rows_len)*100,2) into vpercent
                from index_stats;

                dbms_output.put_line('Deleted        : ' || vpercent || '%');

                if vpercent > 20 then
                    dbms_output.put_line('Recommentation : Consider rebuilding this index as over 20% of the entries are deleted.' );
                    dbms_output.put_line('Action         : alter index '|| i.index_name ||' rebuild online' );
                end if;
            else
                dbms_output.put_line('Deleted        : ' || '0%');
            end if;
        exception
            when others then
                dbms_output.put_line('Error          : ' || SQLERRM);
        end;

        dbms_output.put_line('------------------------------------------------------------------------------------------');
    end loop;
end;
/

spool off;
set linesize 80
set serveroutput off
set verify on
set feedback on
set echo on
quit;

