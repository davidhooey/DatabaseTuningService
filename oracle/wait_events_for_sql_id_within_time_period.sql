-- V$ACTIVE_SESSION_HISTORY
select
    event,
    sum(time_waited) time_waited
from
    v$active_session_history
where
    sql_id = 'SQL_ID'
and
    sample_time between
      to_timestamp('START_TIMESTAMP', 'YYYY-MM-DD HH24:MI:SS.FF3') and
      to_timestamp('END_TIMESTAMP', 'YYYY-MM-DD HH24:MI:SS.FF3')
group by
    event
order by
    time_waited desc;


-- DBA_HIST_SESS_HISTORY
select
    event,
    sum(time_waited) time_waited
from
    dba_hist_active_sess_history
where
    sql_id = 'SQL_ID'
and
   sample_time between
     to_timestamp('START_TIMESTAMP', 'YYYY-MM-DD HH24:MI:SS.FF3') and
     to_timestamp('END_TIMESTAMP', 'YYYY-MM-DD HH24:MI:SS.FF3')
group by
    event
order by
    time_waited desc;
