SQLT Usage
==========

Downloading SQLT
----------------
1. Find a web browser and log into My Oracle Support.
2. Go to the knowledge section and type “SQLT” in the search box. Note 215187.1
   entitled `SQLT (SQLTXPLAIN) - Tool that helps to diagnose a SQL statement
   performing poorly [ID 215187.1]` should be at the top of the list.
3. Scroll to the bottom of the note and choose the version of SQLT suitable
   for your environment. There are currently versions suitable from 9i to 11g.
4. Download the ZIP file.
5. Unzip the zip file.


Installing SQLT
---------------
1. Download the SQLT zip file appropriate for your environment (see above).
2. Unzip the zip file to a suitable location.
3. Navigate to your “install” directory under the unzipped area.
4. Connect as sys, e.g. `sqlplus / as sysdba`
5. Make sure your database is running.
6. Run the `sqcreate.sql` script.
7. Select the default for the first option.
8. Enter and confirm the password for SQLTXPLAIN (the owner of the SQLT
   packages).
9. Select the tablespace where the SQLTXPLAIN will keep its packages and data
  (e.g. USERS).
10. Select the temporary tablespace for the SQLTXPLAIN user (e.g. TEMP).
11. Then enter the username for the user in the database who will use SQLT
    packages to fix tuning problems. Typically this is the schema that runs
    the problematic SQL.
12. Then enter `T`, `D`, or `N`. This reflects your license level for the
    tuning and diagnostics packs. Most sites have both so you would enter
    `T` (this is the default). If you have the diagnostics pack, only enter
    `D`; and if you do not have these licenses, enter `N`.
13. The last message you see is `SQCREATE completed. Installation completed
    successfully`.


Running SQLT
------------
Now that SQLT is installed, it is ready to be used. Remember that installing
the package is done as `SYS` and that running the reports is done as the target
user. Users using the SQLT packages will require the `SQLT_USER_ROLE`. The
target user specified during the installation will have been granted this role.

1. Now exit SQL and change your directory to `.\SQLT\run`. From here log into
   SQL*Plus as the target user.
2. Determine a SQL_ID in need of tuning.
3. Now execute `sqltxtract` passing in the SQL_ID.
```
SQL> @sqltxtract g4pkmrqrgxg3b
```
4. Enter the password for the SQLTXPLAIN (which you entered during the
   installation). The last message you will see if all goes well is `SQLTXTRACT
   completed`.
5. A zip was created in the run directory. Within the zip file is a file called
   `sqlt_s<nnnnn>_main.html`.
