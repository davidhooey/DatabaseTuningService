-------------------------------------------------------------------------------
-- NAME
--     statistics11g.sql
--
-- DESCRIPTION
--     SQL*Plus command file to output the schema statistics for a schema.
--
-- NOTES
--     Execute the script as the schema owner. The results will be written
--     to the following files.
--
--     table_statistics.out
--     table_column_statistics.out
--     table_histograms.out
--     index_statistics.out
--
--     sqlplus SchemaOwner @statistics11g.sql
--
-- REQUIREMENTS
--     Oracle 11g and later.
--
-- HISTORY
--     David Hooey - 2020-10-08
-------------------------------------------------------------------------------

set feedback off
set heading on
set pagesize 50
set termout off
set wrap on

--
-- Column Definitions
--

col column_name               for a30
col endpoint_actual_value     for a100
col global_stats              for a12
col high_value                for a100
col histogram                 for a15
col index_name                for a30
col last_analyzed             for a20
col low_value                 for a100
col object_type               for a12
col partition_name            for a30
col stale_stats               for a11
col stattype_locked           for a15
col subpartition_name         for a30
col table_name                for a30
col table_owner               for a30
col user_stats                for a10

--
-- Table Statistics
--

set linesize 510

spool table_statistics.out

select
    table_name,
    partition_name,
    partition_position,
    subpartition_name,
    subpartition_position,
    object_type,
    num_rows,
    blocks,
    empty_blocks,
    avg_space,
    chain_cnt,
    avg_row_len,
    avg_space_freelist_blocks,
    num_freelist_blocks,
    avg_cached_blocks,
    avg_cache_hit_ratio,
    sample_size,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    global_stats,
    user_stats,
    stattype_locked,
    stale_stats
from
    user_tab_statistics
order by
    table_name,
    partition_position,
    subpartition_position;

spool off

--
-- Table Column Statistics
--

set linesize 400

spool table_column_statistics.out

select
    table_name,
    column_name,
    num_distinct,
    low_value,
    high_value,
    density,
    num_nulls,
    num_buckets,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    sample_size,
    global_stats,
    user_stats,
    avg_col_len,
    histogram
from
    user_tab_col_statistics
order by
    table_name,
    column_name;

spool off

--
-- Index Statistics
--

set linesize 455

spool index_statistics.out

select
    table_name,
    index_name,
    table_owner,
    partition_name,
    partition_position,
    subpartition_name,
    subpartition_position,
    object_type,
    blevel,
    leaf_blocks,
    distinct_keys,
    avg_leaf_blocks_per_key,
    avg_data_blocks_per_key,
    clustering_factor,
    num_rows,
    avg_cached_blocks,
    avg_cache_hit_ratio,
    sample_size,
    to_char(last_analyzed, 'YYYY-MM-DD HH24:MI:SS') last_analyzed,
    global_stats,
    user_stats,
    stattype_locked,
    stale_stats
from
    user_ind_statistics
order by
    table_name,
    index_name;

spool off

--
-- Histogram Statistics
--

set linesize 330

spool table_histograms.out

select
    table_name,
    column_name,
    endpoint_number,
    endpoint_value,
    endpoint_actual_value
from
    user_tab_histograms
order by
    table_name,
    column_name;

spool off

quit
