set long 100000
set longchunksize 100000
set linesize 90

-- 1. Create SQL Performance Analyzer tuning task.
exec :tname := dbms_sqlpa.create_analysis_task(
    sqlset_name => 'MYSTS',
    task_name   => 'MYSPA'
);

-- 2. Build the “before” performance data.
--    execution_type can be TEST EXECUTE or EXPLAIN PLAN
exec dbms_sqlpa.execute_analysis_task
(
    task_name      => :tname,
    execution_type => 'TEST EXECUTE',
    execution_name => 'before'
);

select
    dbms_sqlpa.report_analysis_task(
        task_name => :name,
        type      => 'text',
        section   => 'summary'
    )
from
    dual;

-- 3. Make changes

-- 4. Build the “after” performance data.
--    execution_type can be TEST EXECUTE or EXPLAIN PLAN
exec dbms_sqlpa.execute_analysis_task
(
    task_name      => :tname,
    execution_type => 'TEST EXECUTE',
    execution_name => 'after'
);

select
    dbms_sqlpa.report_analysis_task(
        task_name => :name,
        type      => 'text',
        section   => 'summary'
    )
from
    dual;

-- 5. Generate the compare analysis report.
exec dbms_sqlpa.execute_analysis_task
(
    task_name      => :tname,
    execution_type => 'COMPARE REPORT'
);

select
    dbms_sqlpa.report_analysis_task(
        task_name => :name,
        type      => 'text',
        section   => 'summary'
    )
from
    dual;
